# ThaiExpress
ThaiExpress is a framework for data processing, back test.

## suggested start point
python Main.py Sweep (This may take an hour, depending on the cores of processors)

# Prerequisite Packages
* pandas
* numpy

# Package Structure
## Common
PARAMS.py: This contains ALL the parameter, directory that are used by this package.  
Utils.py: This contains common functions that are used by this package.  
Strategy.py: This is the file for back test. 
ParamRange.py: This provides the parametr sweeping range for different strategies.  
Parallel: For parallel processing of tasks.

## Config
Configurations of logics of different strategies. The most important directory of the package. 

### TSM
TSM is short for Time Series Momentum. This strategy is following the movement of the market
usage: 
* python Config/TSM/TopTSM.py TSM_Classic
* python Weight/TopPortSync.py TSM_Classic
* in ipython, df,s = Utils.funcCalcPort(['TSM_Classic'])

### MFM
MFM is short for Multi-Factor Model.
usage: 
* python Config/MFM/TopMFM.py VarianceMin
* the result is stored in Output/PortfolioTheory/VarianceMin.pickle (as defined in PARAMS.strFileAddressReturnMax)
* a simple version back test for TSM is stored in Config/MFM/TopTSMSimple.py

## Prepdata
concat.py: This helps to concat the dominant contract from .xlsx file. The .xslx file is stored in /Data.
DataAnalysis.py: This helps prepare the equally weighted index, and the summary statistics of the data given.

## Data
Commodity_Data.xlsx: the data set.

## Weight
This directory contains files for attributing weights for different contracts when entering. Currently, two weights-determine modes are supported, including Synchronous and Asynchronous.  

### Synchronous
TopPortSync.py
The weights of all products are adjusted simultaneously in a periodic way. The rebalancing dates are generated in Utils.generateDTTestStartCalendarDay (for TSM) and Utils.generateNthFriday (for TS). Other methods on determining rebalancing dates deserve more efforts.

### Asynchronous
TopPortAsync.py  
The weights of all products are adjusted separately. Whether cu.shf is rebalanced is only dependent on the price of itself. The principle is to make sure 1 sigma of change of cu.shf only results in 1% change of the portfolio. 

# Example
## bacth test for all (Suggested Start Point)
python Main.py Sweep

## bacth test for TSM_Classic (12 months lookbac and 1 month holding)
python Config/TSM/TopTSM.py TSM_Classic.py


