# -*- coding:utf-8 -*-
import pandas as pd
import datetime
import numpy as np
import os, gc, re, sys
from dateutil.parser import parse
from imp import reload

import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS)

# read data
def readRawData():
    #-- read data
    dfTrading = pd.read_excel(PARAMS.strFileAddressXLSX)
    dfTrading['date'] = dfTrading['date'].apply(lambda x: pd.to_datetime(x).to_pydatetime())
    dfTrading = dfTrading.rename(columns={'date':'TradingDay'})
    dfTrading = dfTrading.set_index('TradingDay').sort_index()
    return dfTrading

# summary statistics
def prepSummaryStatistics():
    dfTrading = readRawData()
    #-- statistics of each product
    listDict = []
    for SecuCode in PARAMS.listSecuAll:
        sProduct = dfTrading[SecuCode].dropna().sort_index()
        listDict.append({
            'SecuCode': SecuCode,
            'NumberOfObservation': sProduct.size,
            'AnnualizedReturn': np.power(sProduct.iloc[-1]/sProduct.iloc[0], PARAMS.NTradingDayPerYear/float(sProduct.size)) - 1.,
            'StandardDeviation': sProduct.pct_change().std() * np.sqrt(PARAMS.NTradingDayPerYear),
            'FirstTradingDay': sProduct.index.min(),
            'FirstObservation': sProduct.iloc[0],
            'FirstObservation': sProduct.iloc[0],
            'LastObservation': sProduct.iloc[-1],
            })
    dfSummaryStatistics = pd.DataFrame(listDict).set_index('SecuCode').sort_values(by='FirstTradingDay')
    listPercentage = ['AnnualizedReturn', 'StandardDeviation',]
    listDate = ['FirstTradingDay']
    listDouble = ['NumberOfObservation', 'FirstObservation', 'LastObservation']
    dfPercentage = dfSummaryStatistics[listPercentage] * 100
    dfPercentage = dfPercentage.round(2).astype(str) + '%'
    dfOut = pd.concat([dfPercentage, dfSummaryStatistics[listDouble].round(2).astype(str), dfSummaryStatistics[listDate]], axis=1)
    dfOut = dfOut.rename(columns={'FirstTradingDay':'1st Date', 'NumberOfObservation':'#Obs', 'FirstObservation':'1st Obs', 'LastObservation':'Last Obs', 'AnnualizedReturn':'Annu Ret', 'StandardDeviation': 'Annu Vol',})

    #-- dump 
    with open(PARAMS.strTableAddressSummaryStatistics, 'w') as fh:
        fh.write(dfOut.to_latex())
    return dfOut

def prepCommodityMarketFactor():
    #-- read data
    dfTrading = readRawData()
    dfPCT = dfTrading.pct_change()
    #-- calculate equally weighted index
    sPCTEWIndex = dfPCT.mean(1)
    sVEWIndex = (1+sPCTEWIndex).cumprod()
    dfEWIndex = pd.concat([sPCTEWIndex, sVEWIndex], axis=1)
    dfEWIndex.columns = ['PCT','Value']
    #-- dump
    import matplotlib.pyplot as plt
    dfEWIndex['Value'].plot(grid=True, figsize=(16,9),xlim=(dfEWIndex.index.min(),dfEWIndex.index.max()), rot=90)
    plt.savefig(PARAMS.strFigureAddressEWIndex, format='eps')
    plt.close()
    return dfEWIndex
    
def prepCorrelationMatrix(dfEWIndex=None):
    #-- read data
    listSecuAll = PARAMS.listSecuAll + ['EWIndex']
    dfTrading = readRawData()
    dfPCT = dfTrading.pct_change()
    if dfEWIndex is None:
        dfEWIndex = prepCommodityMarketFactor()
    dfPCT['EWIndex'] = dfEWIndex['PCT']
    # calculate correlation matrix
    listDict = []
    for SecuCodeA in listSecuAll:
        for SecuCodeB in listSecuAll:
            if SecuCodeA == SecuCodeB:
                CC = 1.
            else:
                dfPair = dfPCT[[SecuCodeA, SecuCodeB]].dropna()
                CC= dfPair[SecuCodeA].corr(dfPair[SecuCodeB])
            listDict.append({
                'A': SecuCodeA,
                'B': SecuCodeB,
                'CC': CC,
                })
    dfCC = pd.DataFrame(listDict)
    dfCC = dfCC.pivot_table(index='A', columns='B', values='CC')
    dfCC = dfCC.loc[listSecuAll][listSecuAll]

    #-- dump
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots()
    im = ax.imshow(dfCC)
    ax.set_xticks(np.arange(len(listSecuAll)))
    ax.set_yticks(np.arange(len(listSecuAll)))
    ax.set_xticklabels(listSecuAll)
    ax.set_yticklabels(listSecuAll)
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    # Loop over data dimensions and create text annotations.
    for i in range(len(listSecuAll)):
        for j in range(len(listSecuAll)):
            #text = ax.text(j, i, dfCC.iloc[i, j], ha="center", va="center", color="w")
            pass

    plt.savefig(PARAMS.strFigureAddressCorrelationMatrix, format='eps')
    plt.close()
    return dfCC

if __name__ == '__main__':
    dfSummaryStatistics = prepSummaryStatistics()
    dfEWIndex = prepCommodityMarketFactor()
    dfCorrelation = prepCorrelationMatrix(dfEWIndex)


