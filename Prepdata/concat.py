# -*- coding:utf-8 -*-
import pandas as pd
import datetime
import numpy as np
import os, gc, re, sys
from dateutil.parser import parse
from imp import reload

import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS)

def processData():
    #--- param
    DUMMY = 1e6
    
    # read data
    dfTrading = pd.read_excel(PARAMS.strFileAddressXLSX)
    dfTrading['date'] = dfTrading['date'].apply(lambda x: pd.to_datetime(x).to_pydatetime())
    dfTrading = dfTrading.rename(columns={'date':'TradingDay'})
    dfTrading = dfTrading.set_index('TradingDay').sort_index()
    
    # calculate PCT
    dfPCT = pd.DataFrame()
    for strContractCode in dfTrading.columns:
        print(strContractCode)
        sClose = dfTrading[strContractCode].copy()
        sClose = sClose.ffill()
        sClose = sClose.dropna()
        dfOneContract = pd.DataFrame(sClose)
        dfOneContract.columns = ['Close']
        dfOneContract['Settle'] = dfOneContract['Close']
        dfOneContract['Open'] = dfOneContract['Close'].shift(1).bfill()
        dfOneContract['PCT'] = dfOneContract['Close'].pct_change()
        dfOneContract['Delta'] = dfOneContract['Close'].diff()
        dfOneContract['DeltaSettle'] = dfOneContract['Settle'].diff().fillna(0)
        dfOneContract['Open-PreSettle'] = 0
        dfOneContract['Settle-Open'] = dfOneContract['DeltaSettle']
        dfOneContract['Value'] = dfOneContract['Close'] / dfOneContract['Close'].dropna().values[0]
        dfOneContract['ContractCode'] = strContractCode
        dfOneContract['SecuCode'] = strContractCode
        dfOneContract = dfOneContract.fillna(0)
        dfOneContract = dfOneContract.dropna()
        dfPCT = dfPCT.append(dfOneContract)
    dfPCT['TurnoverVolume'] = DUMMY
    
    # scale OHLC
    dfResult = dfPCT
    dfResult['OpenRaw'] = dfResult['Open']
    dfResult['SettleRaw'] = dfResult['Settle']
    dfResult['CloseRaw'] = dfResult['Close']
    dfResult['Open'] = dfResult['Open'] / dfResult['Close'] * dfResult['Value']
    dfResult['Settle'] = dfResult['Settle'] / dfResult['Close'] * dfResult['Value']
    dfResult['Close'] = dfResult['Value']
    dfResult['High'] = DUMMY
    dfResult['Low'] = DUMMY
    dfResult['boolDominantChange'] = False
    dfResult = dfResult.reset_index().set_index(['SecuCode', 'TradingDay'])
    dfResult = dfResult[dfResult.index.get_level_values('SecuCode').isin(PARAMS.listSecuAll)]
    dfResult.to_pickle(PARAMS.strFileAddressExe)
    
if __name__ == '__main__' :
    processData()
