# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
import pdb, datetime, os, shutil, random, time, gc, re, sys
from dateutil.parser import parse
from imp import reload
import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS)
import ThaiExpress.Prepdata.DataAnalysis as DataAnalysis
reload(DataAnalysis)

#####################################3
#-- param
#####################################3
NMonthLookBack = 12
NMonthTrain = NMonthLookBack + 1
NMonthHolding = 1
FutureReturn = 'FutureReturn'
SingleContractLimit = 0.2

#####################################3
#-- read data
#####################################3
dfTrading = DataAnalysis.readRawData()
dfPCT = dfTrading.pct_change().dropna()

#####################################3
#-- back test for each month
#####################################3
#-- prep
listSPCT = []
listSPosition = []

#-- generate listDTMonthEnd, whose close prices are used to adjust position.
dfDT = dfTrading.copy()
dfDT = dfDT[dfDT.index.weekday==0]
sDTMonthEnd = dfDT.reset_index().groupby(dfDT.index.strftime('%Y%m'))['TradingDay'].last()
for nMonth in range(NMonthTrain, sDTMonthEnd.size-NMonthHolding, NMonthHolding):
    # get data for MFM
    dtStart = sDTMonthEnd.iloc[nMonth-NMonthTrain]
    dtEnd = sDTMonthEnd.iloc[nMonth]
    dtFuture = sDTMonthEnd.iloc[nMonth+NMonthHolding]
    dfTrain = dfTrading[(dfTrading.index>=dtStart)&(dfTrading.index<dtEnd)]
    dfTest = dfTrading[(dfTrading.index>=dtEnd)&(dfTrading.index<=dtFuture)]
    print(dtEnd)

    # regression for factor return
    nTradingDay = dfTrading.index.get_loc(dtEnd)
    dfLastMonth = dfTrading.iloc[nTradingDay-NMonthLookBack*20:nTradingDay]
    sDirection = dfLastMonth.iloc[-1] / dfLastMonth.iloc[0] - 1
    listSecuCode = sDirection.dropna().index.values.tolist()

    # weight, inverse of volatility
    weightInverseVolatility = dfTrain[listSecuCode].pct_change().tail(60).std().apply(lambda x: 1./x)
    weightInverseVolatility = weightInverseVolatility / weightInverseVolatility.sum()
    weightInverseVolatility = weightInverseVolatility * sDirection.apply(np.sign)
    weight = weightInverseVolatility

    # calculate future month return
    sPCT=(dfTest[listSecuCode].pct_change().dropna() * weight).sum(1)
    listSPCT.append(sPCT)
    
#-- dump result
sPCT = pd.concat(listSPCT, axis=0)

