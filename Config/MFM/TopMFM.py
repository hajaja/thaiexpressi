# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
import pdb, datetime, os, shutil, random, time, gc, re, sys
from dateutil.parser import parse
from imp import reload
import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS)
import ThaiExpress.Prepdata.DataAnalysis as DataAnalysis
reload(DataAnalysis)

def top(strMFMType):
    """
    Calculate daily value of portfolio, generated based on Multi-Factor Model.
    CPLEX is necessary for this function.

    Parameters:
    ----------
    * strMFMType:
    'ReturnMax': maximize expected return; 
    'VarianceMin': minimize variance of portfolio.
                                            
    Returns:
    -------
    None
    """
    #####################################3
    #-- param
    #####################################3
    #strMFMType = 'ReturnMax'
    if strMFMType == 'ReturnMax':
        NMonthHistory = 60
    elif strMFMType == 'VarianceMin':
        NMonthHistory = 13
    
    NDayVolatilityLookBack = 60
    NMonthHolding = 1
    FutureReturn = 'FutureReturn'
    SingleContractLimit = 0.4
    SingleSectorLimit = 0.4
    MICRO_ADJUST_AMPLITUDE = 0.5
    listNMonthLookBack = [1,3,11]
    KeyMOM = 'MOM11'
    
    #####################################3
    #-- read data
    #####################################3
    dfTrading = DataAnalysis.readRawData()
    dfPCT = dfTrading.pct_change().dropna()
    
    #####################################3
    #-- back test for each month
    #####################################3
    #-- prep
    listSPCT = []
    listSPosition = []
    
    #-- generate listDTMonthEnd
    dfDT = dfTrading.copy()
    dfDT = dfDT[dfDT.index.weekday==0]
    sDTMonthEnd = dfDT.reset_index().groupby(dfDT.index.strftime('%Y%m'))['TradingDay'].last()
    listX = ['MOM%d'%NMonthLookBack for NMonthLookBack in listNMonthLookBack]
    NMonthLookBackMax = max(listNMonthLookBack)
    for nMonth in range(NMonthHistory, sDTMonthEnd.size-NMonthHolding, NMonthHolding):
        # get data for MFM
        dtStart = sDTMonthEnd.iloc[nMonth-NMonthHistory]
        dtEnd = sDTMonthEnd.iloc[nMonth]
        dtFuture = sDTMonthEnd.iloc[nMonth+NMonthHolding]
        dfTrain = dfTrading[(dfTrading.index>=dtStart)&(dfTrading.index<=dtEnd)]
        dfTest = dfTrading[(dfTrading.index>=dtEnd)&(dfTrading.index<=dtFuture)]
        print(dtEnd)
    
        # prepare X Y
        dfTrainMonthly = dfTrain.loc[sDTMonthEnd.iloc[nMonth-NMonthHistory:nMonth+1]].dropna(axis=1)
        listSecuCode = dfTrainMonthly.columns.tolist()
        listDFXY = []
        for SecuCode in dfTrainMonthly.columns:
            df = pd.DataFrame(dfTrainMonthly[SecuCode])
            df.columns = ['Close']
            df[FutureReturn] = df['Close'].shift(-1) / df['Close'] - 1
            for NMonthLookBack in listNMonthLookBack:
                df['MOM%d'%NMonthLookBack] = df['Close']/df['Close'].shift(NMonthLookBack) - 1
            df['SecuCode'] = SecuCode
            df['Var'] = df['Close'].pct_change().var()
            listDFXY.append(df)
        dfXY = pd.concat(listDFXY, axis=0)
        dfLastMonth = dfXY[dfXY.index==dtEnd].reset_index().set_index('SecuCode')
        
        # weight, inverse of volatility
        weightInverseVolatility = dfTrain[listSecuCode].pct_change().tail(NDayVolatilityLookBack).std().apply(lambda x: 1./x)
        weightInverseVolatility = weightInverseVolatility / weightInverseVolatility.sum()
        weightInverseVolatility = weightInverseVolatility * dfLastMonth[KeyMOM].apply(np.sign)
    
        if strMFMType == 'ReturnMax':
            # factor return
            import statsmodels.api as sm
            boolSignificant = (dfXY[FutureReturn] > dfXY[FutureReturn].quantile(0.7)) | (dfXY[FutureReturn] < dfXY[FutureReturn].quantile(0.3))
            W = dfXY[boolSignificant].dropna()['Var'].apply(lambda x: 1./x)
            reg = sm.WLS(dfXY[boolSignificant].dropna()[FutureReturn].values, dfXY[boolSignificant].dropna()[listX].values, W).fit()
            coef = reg.params
            residual = reg.resid
            residual = pd.Series(residual, index=dfXY[boolSignificant].dropna().index)
            print("%s, Mean squared error: %.2f, R2: %.2f"%(dtEnd, reg.mse_model, reg.rsquared))
            sVecF = pd.Series(coef, index=listX)
            sVecU = residual
    
        # calculate covariance
        dfCov = dfTrain[listSecuCode].pct_change().cov()
    
        # formulate the optimization
        from docplex.mp.advmodel import AdvModel as Model
        mdl = Model(name='portfolio_miqp')
        mdl.set_log_output('CPLEX_LOG.lg')
        mdl.set_output_level(1)
        mdl.float_precision = 6
        mdl.parameters.simplex.tolerances.feasibility.set(1e-7)
        
        # add variables 
        boolLong = dfLastMonth[KeyMOM] > 0
        boolShort = dfLastMonth[KeyMOM] <= 0
        dfLastMonth.loc[boolLong, 'frac'] = mdl.continuous_var_list(dfLastMonth[boolLong].index.values.tolist(), name='frac', lb=0, ub=SingleContractLimit)
        dfLastMonth.loc[boolShort, 'frac'] = mdl.continuous_var_list(dfLastMonth[boolShort].index.values.tolist(), name='frac', lb=-SingleContractLimit, ub=0)
        for SecuCode, weight in weightInverseVolatility.items():
            if weight > 0:
                mdl.add_constraint(dfLastMonth.loc[SecuCode,'frac'] <= weight * (1+MICRO_ADJUST_AMPLITUDE))
                mdl.add_constraint(dfLastMonth.loc[SecuCode,'frac'] >= weight * (1-MICRO_ADJUST_AMPLITUDE))
            else:
                mdl.add_constraint(dfLastMonth.loc[SecuCode,'frac'] <= weight * (1-MICRO_ADJUST_AMPLITUDE))
                mdl.add_constraint(dfLastMonth.loc[SecuCode,'frac'] >= weight * (1+MICRO_ADJUST_AMPLITUDE))
        
        # constraints - sum of fractions equal 100%
        sumWeightLong = mdl.sum(dfLastMonth.loc[boolLong, 'frac'])
        sumWeightShort = mdl.sum(dfLastMonth.loc[boolShort, 'frac'])
        mdl.add_constraint((sumWeightLong-sumWeightShort) == 1.)
    
    
        for Sector in ['Metal', 'Gas', 'Wheat', 'Cattle']:
            listMetal = dfLastMonth.index.intersection(PARAMS.dictSectorProduct[Sector])
            if len(listMetal) > 0:
                sumMetal = mdl.sum(dfLastMonth.loc[listMetal, 'frac'])
                mdl.add_constraint(sumMetal <= SingleSectorLimit)
                mdl.add_constraint(sumMetal >= -SingleSectorLimit)
        
        #-- objective
        fracs = dfLastMonth['frac']
        variance = mdl.sum(dfCov[sec1][sec2] * fracs[sec1] * fracs[sec2] for sec1 in listSecuCode for sec2 in listSecuCode)
        actual_return = mdl.dot(dfLastMonth['frac'], np.dot(dfLastMonth[listX].values, sVecF))
        if strMFMType == 'VarianceMin':
            mdl.minimize(variance)
            strFileAddressDump = PARAMS.strFileAddressVarianceMin
        elif strMFMType == 'ReturnMax':
            target_variance = np.power(0.1, 2) / 261.
            ct_variance = mdl.add_constraint(variance <= target_variance, ctname='UB_variance')
            mdl.maximize(actual_return)
            strFileAddressDump = PARAMS.strFileAddressReturnMax
        else:
            raise Exception
        
        # solve
        try:
            assert mdl.solve(url=None, key=None), "Solve failed"
            # read weight
            listDictWeight = []
            for SecuCode, row in dfLastMonth.iterrows():
                pct = row['frac'].solution_value
                listDictWeight.append({'SecuCode': row.name, 'Weights': pct})
            weight = pd.DataFrame(listDictWeight).set_index('SecuCode')['Weights']
            weight.name = 'weight'
            print(variance.solution_value, actual_return.solution_value, weight)
        except:
            # use inverse volatility as default weight
            weight = weightInverseVolatility
            weight.name = 'weight'
    
        # calculate future month return
        sPCT=(dfTest[listSecuCode].pct_change().dropna() * weight).sum(1)
        listSPCT.append(sPCT)
        
    #-- dump result
    sPCT = pd.concat(listSPCT, axis=0)
    sPCT.to_pickle(strFileAddressDump)

if __name__ == '__main__':
    strMFMType = sys.argv[1]
    top(strMFMType)

