# -*- coding: utf-8 -*-

def funcPerTask(dictDataSpec):
    # import 
    from imp import reload
    import pandas as pd
    import numpy as np
    import os, gc, re, sys, datetime
    from dateutil.parser import parse
    import ThaiExpress.Common.Utils as Utils
    reload(Utils)
    import ThaiExpress.Common.PARAMS as PARAMS
    reload(PARAMS)
    import ThaiExpress.Common.Strategy as Strategy
    reload(Strategy)
    import ThaiExpress.Config.TSM.UtilsTSM as UtilsTSM
    reload(UtilsTSM)

    # param
    dictParam = dict(PARAMS.dictParamTemplate)

    # prepare directory
    strFileAddressSignal = dictDataSpec['strFileAddress']
    if os.path.exists(strFileAddressSignal):
        return

    # read data
    dfAll = Utils.getTradingDataPoint_Commodity(dictDataSpec).replace(np.inf, np.nan)
    dtLastObservation = dfAll.index[-1]
    dtEnter = PARAMS.dtEnter
    rowLast = dfAll.ix[dtLastObservation]
    rowLast.name = dtEnter
    dfAll = dfAll.append(rowLast)
    dictDataSpec['df'] = dfAll

    # set time start, end, step
    dtDataStart = dfAll.index[0]
    dtDataEnd = dfAll.index[-1]
    NDayTrain = dictDataSpec['NMonthTrain'] * 20
    NMonthTest = dictDataSpec['NMonthTest']
    NDayShift = dictDataSpec['NDayShift']
    NMonthStart = dictDataSpec['NMonthStart']
    NthFriday = dictDataSpec['NthFriday']
    
    # get listDTTestStart
    listDTTestStart = Utils.generateNthFriday(NthFriday, NMonthStart, NDayShift, NMonthTest)
    
    # prepare loop
    dtDataStart = datetime.datetime.combine(dfAll.index[0], dtDataStart.time())
    dtTrainStart = dtDataStart
    seriesIndicatorAll = pd.Series()
    seriesYAll = pd.Series()

    seriesIndicatorAll = dfAll['Close'].copy()
    seriesIndicatorAll = seriesIndicatorAll.apply(lambda x: np.nan)

    # back test this data spec
    if dtEnter > listDTTestStart[-1]:
        listDTTestStart.append(dtEnter)
    for nWindow, dtTestStart in enumerate(listDTTestStart):
        # the start & end of test window
        dtTestStart = listDTTestStart[nWindow]
        if nWindow < len(listDTTestStart)-1:
            dtTestEnd = listDTTestStart[nWindow + 1]
        else:
            dtTestEnd = dtEnter
            if dtTestEnd < dtTestStart:
                break

        # the start & end of train window
        dtTrainEnd = dtTestStart
        if dfAll[dfAll.index < dtTestStart].index.size < NDayTrain:
            continue
        else:
            dtTrainStart = dfAll[dfAll.index < dtTestStart].index[-NDayTrain]
        
        # get the train window and test window
        dfTrain = dfAll[(dfAll.index>=dtTrainStart)&(dfAll.index<dtTrainEnd)].copy()
        dfTest = dfAll[(dfAll.index>=dtTestStart)&(dfAll.index<dtTestEnd)].copy()
        
        if dfTrain.empty:
            continue
            
        # determine the trend 
        dictParamTSM = {
                'df': dfTrain,
                'strMethodTrend': dictDataSpec['strMethodTrend'],
                'NDayTrain': NDayTrain,
                'NDaySkip': dictDataSpec['NDaySkip'],
                }
        trend = UtilsTSM.determineTrend(dictParamTSM)
        
        if dfTrain['Volume'].mean() < PARAMS.NThresholdVolume:
            trend = 0
        elif dfTrain.empty is False and dfTrain.ix[dfTrain.index[-1], 'Volume'] < 100:
            trend = 0
        seriesIndicatorAll.ix[dfTrain.index[-1]] = trend    # in Strategy.py, there will be 1 bar shift

        print(dtTrainStart, dtTrainEnd, dtTestStart, dtTestEnd, trend)
    
    ########################################################
    # evaluate the strategy (the indicator generated)
    ########################################################
    # evaluate
    reload(Strategy)
    dictParam['boolStoploss'] = dictDataSpec['boolStoploss']
    strategy = Strategy.Strategy(dfAll.ix[seriesIndicatorAll.index], dictParam)
    strategy.seriesYAll = seriesYAll
    strategy.df['indicator'] = seriesIndicatorAll
    dictResultS = strategy.evaluateLongShortSimplified()
    
    # dominant contract
    indexDominantChange = dfAll[dfAll['boolDominantChange']==True].index
    indexDominantChange = indexDominantChange & strategy.seriesReturnPCTHoldDaily[strategy.seriesReturnPCTHoldDaily!=0].index
    strategy.seriesReturnPCTHoldDaily.ix[indexDominantChange] = strategy.seriesReturnPCTHoldDaily[indexDominantChange] - dictParam['commission']

    # for memory
    if dictDataSpec['boolStoploss']:
        strategy.df = strategy.df[['Close', 'indicator', 'returnPCTHold', 'indicatorOfDecision', 'Stoploss', 'returnStoploss', 'StoplossPrice', 'StoplossPriceTomorrow']]
    else:
        strategy.df = strategy.df[['Close', 'indicator', 'returnPCTHold', 'indicatorOfDecision']]

    # add to dict
    dictDataSpec['strategy'] = strategy

    # save data
    sDataSpec = pd.Series(dictDataSpec)
    sDataSpec.to_pickle(strFileAddressSignal)
