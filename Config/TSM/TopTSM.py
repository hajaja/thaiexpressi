# -*- coding:utf-8 -*-
import pandas as pd
import datetime
import numpy as np
import os
import re
import sys
from imp import reload

import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS) 
from ThaiExpress.Config.TSM import UtilsTSM
reload(UtilsTSM)
from ThaiExpress.Config.TSM import API
reload(API)

def top(strParamSweep):
    """
    Calculate daily value of portfolio, based on synchronous rebalancing style
    
    Parameters:
    ----------
    * strParamSweep
    Please refer to Common/ParamRange.py for more parameter set
                                            
    Returns:
    -------
    None
    """
    ########################################################
    # generate listDictDataSpec for backtest
    ########################################################
    dictDataSpec = dict(PARAMS.dictDataSpecTemplate)
    dictDataSpec['strModelName'] = 'TSM'
    dictDataSpec['freq'] = '1day'
    listDictDataSpec = Utils.sweepParam(dictDataSpec, strParamSweep)
    listDictDataSpec = Utils.sprawnDictPerCase(listDictDataSpec)
    
    ########################################################
    # back test each data spec
    ########################################################
    dictDictChunk = {}
    for nSpec, dictDataSpec in enumerate(listDictDataSpec):
        print(nSpec, len(listDictDataSpec), dictDataSpec['strCase'])
        API.funcPerTask(dictDataSpec)
        dictDictChunk[nSpec] = dictDataSpec
    
    ########################################################
    # submit jobs
    ########################################################
    import ThaiExpress.Common.Parallel as Parallel
    reload(Parallel)
    dictOptionDefault = {
            'ncpus': PARAMS.ncpus,
            'funcTask': API.funcPerTask,
            }
    #Parallel.Utils.submitjobs(dictDictChunk, dictOptionDefault)
    
    ########################################################
    # submit jobs weight
    ########################################################
    import ThaiExpress.Weight.UtilsPortSync as UtilsPortSync
    reload(UtilsPortSync)
    dfOut = UtilsPortSync.funcTopPort(strParamSweep)

if __name__ == '__main__':
    strParamSweep = sys.argv[1]
    if PARAMS.boolClearData:
        Utils.funcClearData(strParamSweep)
    top(strParamSweep)

