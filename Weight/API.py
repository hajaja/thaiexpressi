# -*- coding: utf-8 -*-
def funcPerTaskSync(dictPerTopPort):
    #----- import 
    from imp import reload
    import pandas as pd
    import numpy as np
    from dateutil.parser import parse
    import os, re, pdb, datetime, random, gc
    import ThaiExpress.Common.Utils as Utils
    reload(Utils)
    import ThaiExpress.Common.PARAMS as PARAMS
    reload(PARAMS)
    import ThaiExpress.Weight.UtilsPortSync as UtilsPortSync
    reload(UtilsPortSync)
    
    # param
    strMethodVolatility = 'EWMAN2'

    #-- 
    gc.collect()
    listDictDataSpec = []
    for strFileAddress in dictPerTopPort['listFileAddress']:
        listDictDataSpec.append(pd.read_pickle(strFileAddress))
    #-- save config file of the port
    dictPerTopPortOut = {}
    for key, value in dictPerTopPort.items():
        if key not in ['strCase', 'listFileAddress', 'Secu']:
            if type(value) is not list:
                dictPerTopPortOut[key] = value
            else:
                dictPerTopPortOut[key] = value[0]
    strFileAddressPortConfig = '%s/portconfig.pickle'%dictPerTopPort['dirCasePort']
    pd.Series(dictPerTopPortOut).to_pickle(strFileAddressPortConfig)

    NDayVolatilityLookBack = dictPerTopPort['NDayVolatilityLookBack']

    ######################################################### 
    # determine the rebalance datetime
    ######################################################### 
    listDTTestStart = Utils.generateNthFriday(dictPerTopPort['NthFriday'], dictPerTopPort['NMonthStart'], dictPerTopPort['NDayShift'], dictPerTopPort['NMonthTest'])
    
    dtEnter = PARAMS.dtEnter
    listDTTestStart.append(dtEnter)
    seriesDTRebalance = pd.Series(listDTTestStart)
    
    ######################################################### 
    # calculate the portfolio return
    ######################################################### 
    dtBackTestStart = seriesDTRebalance[seriesDTRebalance>=PARAMS.dtDataStart].values[0]
    dfOut = UtilsPortSync.funcShowStrategyPortSum(
            dictPerTopPort,
            listDictDataSpec, 
            seriesDTRebalance, 
            strMethodVolatility, 
            NDayVolatilityLookBack, 
            dtBackTestStart
            )
    dfOut = dfOut[dfOut.index >= dtBackTestStart]
    # plot & savefig
    dfOut = dfOut[['Cum Return', 'Max DD', 'Position']].ffill().dropna()
    # output
    dfOut.to_pickle('%s/%s'%(dictPerTopPort['dirCasePort'], 'dfOut.pickle'))

