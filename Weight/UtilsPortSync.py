# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
from dateutil.parser import parse
import os, re, pdb, datetime, random, sys
from imp import reload

import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS)
from . import API
reload(API)

############################################################
# weight param set
############################################################
dictDictWeightParamSet = {}
dictDictWeightParamSet['TSM_Classic'] = {
        'NDayVolatilityLookBack': 60,
        'UpperLimitOneDirection': 1.,
        'UpperLimitOneSector': 1.,
        }
dictDictWeightParamSet['TSM_SensitivityReduction'] = {
        'NDayVolatilityLookBack': 60,
        'UpperLimitOneDirection': 0.5,
        'UpperLimitOneSector': 1.,
        }
dictDictWeightParamSet['TSM_SectorLimit'] = {
        'NDayVolatilityLookBack': 60,
        'UpperLimitOneDirection': 0.5,
        'UpperLimitOneSector': 0.3,
        }
def getDictWeightParamSet(strParamSweep):
    if strParamSweep in dictDictWeightParamSet:
        return dictDictWeightParamSet[strParamSweep]
    else:
        return dictDictWeightParamSet['TSM_Classic']

############################################################
# calculate weighted port performance
############################################################
def funcShowStrategyPortSum(
        dictPerTopPort,
        listDictDataSpec, 
        seriesDTRebalance, 
        strMethodVolatility, 
        NDayVolatilityLookBack, 
        dtBackTestStart
    ):
    
    # extract the selected data
    listSeriesClose = []
    listSeriesReturn = []
    listSeriesPosition = []
    listSeriesIndicator = []
    listSeriesStoploss = []
    listSeriesReturnStoploss = []
    listStoplossPrice = []
    listStoplossPriceTomorrow = []
    for dictDataSpec in listDictDataSpec:
        strategy = dictDataSpec['strategy']
        if dictDataSpec['Secu'] not in PARAMS.listSecuAll:
            continue
        
        seriesVolatility = strategy.df['indicator'].apply(lambda x: np.nan)
        seriesPosition = strategy.df['indicator'].apply(lambda x: np.nan)
        dfAll = strategy.df
        listDTTestStart = seriesDTRebalance

        seriesReturn = strategy.seriesReturnPCTHoldDaily
        seriesReturn.name = dictDataSpec['Secu']
        listSeriesReturn.append(seriesReturn)

        seriesClose = dfAll['Close']
        seriesClose.name = dictDataSpec['Secu']
        listSeriesClose.append(dfAll['Close'].copy())
        
        seriesIndicator = strategy.df['indicator'].copy()
        seriesIndicator.name = dictDataSpec['Secu']
        listSeriesIndicator.append(seriesIndicator)

        # stoploss price
        if 'Stoploss' in strategy.df.columns:
            series = strategy.df['Stoploss']
            series.name = dictDataSpec['Secu']
            listSeriesStoploss.append(series)
            series = strategy.df['returnStoploss']
            series.name = dictDataSpec['Secu']
            listSeriesReturnStoploss.append(series)
            seriesStoplossPrice = strategy.df['StoplossPrice'].copy()
            seriesStoplossPrice.name = dictDataSpec['Secu']
            listStoplossPrice.append(seriesStoplossPrice)
            series = strategy.df['StoplossPriceTomorrow']
            series.name = dictDataSpec['Secu']
            listStoplossPriceTomorrow.append(series)
            boolStoploss = True
        else:
            boolStoploss = False

    # concat the extracted data
    print('concat data')
    dfClose = pd.concat(listSeriesClose, axis=1).fillna(0)
    dfClose.index = dfClose.index.to_pydatetime()
    dfClose.index.name = 'dtEnd'
    
    dfReturn = pd.concat(listSeriesReturn, axis=1).fillna(0)
    dfReturn.index.name = 'dtEnd'
    dfReturn = dfReturn.reset_index()
    dfReturn['dtEnd'] = dfReturn['dtEnd'].apply(lambda x: datetime.datetime.combine(x, datetime.time(0,0)))
    dfReturn = dfReturn.set_index('dtEnd').sort_index()
    dfReturn.ix[dfReturn.index[-1], dfReturn.columns] = 0
    dfIndicator = pd.concat(listSeriesIndicator, axis=1)    # indicator everyday
    dfIndicatorAtAction = dfIndicator.ix[seriesDTRebalance] # when to change direction

    # calculate volatility, and replace 0 return in dfReturn with np.nan
    print('calculate volatility data')
    listDTTestStart = seriesDTRebalance.tolist()
    listDictVolatility = []
    dfReturnValid = pd.DataFrame()
    for nWindow in range(0, len(listDTTestStart)) :
        listReturn = []
        for SecuCode in dfReturn.columns:
            sDailyReturnAll = dfReturn[SecuCode]
            sDailyCloseAll = dfClose[SecuCode]
            
            # get the train window and test window
            dtTestStart = listDTTestStart[nWindow]
            dtTrainEnd = dtTestStart
            if nWindow == 0:
                continue
            else:
                dtTrainStart = listDTTestStart[nWindow-1]

            # is there any trade 
            sDailyReturn = sDailyReturnAll[(sDailyReturnAll.index>=dtTrainStart) & (sDailyReturnAll.index<dtTrainEnd)].copy()
            sDailyClose = sDailyCloseAll[(sDailyCloseAll.index>=dtTrainStart) & (sDailyCloseAll.index<dtTrainEnd)].copy()
            if sDailyReturn.empty:
                continue

            # calculate volatility & weight
            volatility = sDailyCloseAll[sDailyCloseAll.index<dtTrainEnd].pct_change().tail(NDayVolatilityLookBack).std() * np.sqrt(PARAMS.NTradingDayPerYear)
            weight = 0.1 / volatility
            
            if sDailyReturn.sum() == 0:
                # not in the top 20% of XSM
                sDailyReturn = sDailyReturn.apply(lambda x: np.nan)

            listReturn.append(sDailyReturn)
            listDictVolatility.append({'dtTestStart': dtTestStart, 'SecuCode':SecuCode, 'volatility': volatility, 'weight': weight})

        if len(listReturn) == 0:
            continue
        else:
            dfReturnValid = dfReturnValid.append(pd.concat(listReturn, axis=1))
    
    # append tomorrow index to ReturnValid 
    ######
    # diff between dfReturnValid and dfReturn
    # when there is no position for a secu, dfReturnValid is 0 * dfReturn
    ######
    print('calculate daily weight')
    dfReturnValid = dfReturnValid.append(dfIndicator.ix[-1].replace(0, np.nan))

    # position of every day
    dfVolatility = pd.DataFrame(listDictVolatility)
    dfVolatility = dfVolatility.set_index('dtTestStart').sort_index()
    dfWeightOriginal = dfVolatility.reset_index().pivot(index='dtTestStart', columns='SecuCode', values='weight')
    dfWeight = dfWeightOriginal.ix[dfReturnValid.index].ffill()
    dfWeight.index.name = dfReturnValid.index.name

    # adjust weight to meet leverage limit
    dfSelected = dfReturnValid.applymap(lambda x: ~np.isnan(x)).astype(np.int)
    dfWeightValid = dfWeight * dfSelected
    positionUpper = PARAMS.LEVERAGE
    sTotalWeight = dfWeightValid.sum(1)
    for Secu in dfWeightValid.columns:
        dfWeightValid[Secu] = dfWeightValid[Secu] / sTotalWeight * positionUpper 
        dfWeightValid[Secu] = dfWeightValid[Secu].apply(lambda x: min(x, PARAMS.UpperPositionSingleContract))
        pass
    
    # upper limit for sector
    UpperLimitOneSector = dictPerTopPort['UpperLimitOneSector']
    dfWeightDirection = dfWeightValid * dfIndicator
    for Sector in PARAMS.dictSectorProduct.keys():
        listProduct = PARAMS.dictSectorProduct[Sector]
        sWeightDirection = dfWeightDirection[listProduct].sum(axis=1)
        sWeightDirectionABS = sWeightDirection.abs()
        sWeightAdjustment = sWeightDirectionABS.apply(lambda x: 1. if x < UpperLimitOneSector else x / UpperLimitOneSector)
        for SecuCode in listProduct:
            dfWeightValid[SecuCode] = dfWeightValid[SecuCode] / sWeightAdjustment

    # upper limit for one-direction, adjust weight to reduce correlation with the Equally-Weighted Market Index
    dfWeightDirection = dfWeightValid * dfIndicator
    sWeightDirection = dfWeightDirection.sum(axis=1)
    sWeightDirectionABS = sWeightDirection.abs()
    UpperLimitOneDirection = dictPerTopPort['UpperLimitOneDirection']
    sWeightAdjustment = sWeightDirectionABS.apply(lambda x: 1. if x < UpperLimitOneDirection else x / UpperLimitOneDirection)
    for SecuCode in dfWeightValid.columns:
        dfWeightValid[SecuCode] = dfWeightValid[SecuCode] / sWeightAdjustment

    # if some secu's weight is smaller than one contract, then do not trade
    TotalMoney = PARAMS.TOTALMONEY 
    dfPositionDollar = dfWeightValid * TotalMoney
    listPositionContract = []
    listPositionDecimal = []
    for SecuCode in dfPositionDollar.columns:
        listColumn = ['CloseRaw', 'SettleRaw']
        dfSecu = Utils.getDFOneProduct(SecuCode, listColumn)
        sPositionDollar = dfPositionDollar[SecuCode]
        sSettleRaw = dfSecu.ix[sPositionDollar.index, 'SettleRaw'].shift(1)
        sSettleRaw.name = SecuCode
        sNominalContract = sSettleRaw * Utils.dfDetail.ix[SecuCode, 'multiplier']
        sPositionContract = sPositionDollar / sNominalContract
        sPositionContract = sPositionContract.apply(lambda x: np.round(x))
        sPositionDecimal = sPositionContract * sNominalContract / TotalMoney
        sPositionContract.name = SecuCode
        sPositionDecimal.name = SecuCode
        listPositionContract.append(sPositionContract)
        listPositionDecimal.append(sPositionDecimal)
    dfPositionContract = pd.concat(listPositionContract, axis=1)
    dfPositionDirection = (dfIndicatorAtAction*dfPositionContract).ffill()
    dfPosition = pd.concat(listPositionDecimal, axis=1)
    dfWeightValid = dfPosition

    # daily position
    seriesDailyPosition = dfWeightValid.sum(axis=1)
    seriesDailyPosition = seriesDailyPosition[seriesDailyPosition.index >= dtBackTestStart]
    seriesDailyPosition.name = 'Position'

    # calculate daily return as in WH
    print('calculate daily return WH')
    listEquityDeltaDaily = []
    for SecuCode in dfPositionDirection.columns:
        listColumn = ['OpenRaw', 'CloseRaw', 'SettleRaw', 'DeltaSettle', 'Open-PreSettle', 'Settle-Open']
        #listColumn = ['CloseRaw', 'SettleRaw', 'DeltaSettle']
        dfSecu = Utils.getDFOneProduct(SecuCode, listColumn)
        dfTrading = dfSecu
        dfTrading = dfTrading.rename({'OpenRaw':'Open', 'CloseRaw':'Close', 'SettleRaw':'Settle'})
        # contracts in and out
        sPositionDirection = dfPositionDirection[SecuCode]
        sPositionDirectionDiff = sPositionDirection.diff()
        sPositionIn = sPositionDirectionDiff.copy()
        sPositionOut = -sPositionDirectionDiff.copy()
        sPositionIn.ix[sPositionDirection.abs() < sPositionDirection.shift(1).abs()] = 0
        sPositionOut.ix[sPositionDirection.abs() > sPositionDirection.shift(1).abs()]  = 0

        # calculate daily equity delta
        sEquity = sPositionDirection * dfTrading['DeltaSettle'] - sPositionIn * dfTrading['Open-PreSettle'] + sPositionOut * dfTrading['Open-PreSettle']
        #sEquity = sEquity - (sPositionIn.abs() + sPositionOut.abs()) * dfTrading['OpenRaw'] * PARAMS.COMMISSION
        sEquity = sEquity - (sPositionIn.abs() + sPositionOut.abs()) * dfTrading['CloseRaw'] * PARAMS.COMMISSION
        sEquity = sEquity * Utils.dfDetail.ix[SecuCode, 'multiplier']
        sEquity.name = SecuCode
        listEquityDeltaDaily.append(sEquity)

    dfEquityDeltaDaily = pd.concat(listEquityDeltaDaily, 1)
    dfEquityDeltaDaily['Total'] = dfEquityDeltaDaily.sum(1)

    # calculate contribution per sector / per product
    sContribution = dfEquityDeltaDaily[dfEquityDeltaDaily.index > dtBackTestStart].sum(0)
    strFileAddressContrib = '%s/%s.pickle'%(dictPerTopPort['dirCasePort'], 'contrib')
    sContribution.to_pickle(strFileAddressContrib)
    
    # calculate daily equity PCT
    ixCommon = dfEquityDeltaDaily.index.intersection(seriesDTRebalance)
    for dt in ixCommon:
        dfEquityDeltaDaily.ix[dt, 'NDTRebalance'] = dt.strftime('%Y%m%d')
    dfEquityDeltaDaily['NDTRebalance'] = dfEquityDeltaDaily['NDTRebalance'].ffill()
    def funcCalcEquityPCT(sEquity):
        sEquity = sEquity.cumsum() + PARAMS.TOTALMONEY
        sPCT = sEquity.pct_change()
        sPCT.ix[0] = sEquity.ix[0] / PARAMS.TOTALMONEY - 1
        return sPCT
    sEquityPCT = dfEquityDeltaDaily.groupby('NDTRebalance')['Total'].apply(funcCalcEquityPCT)
    seriesDailyReturn = sEquityPCT
    seriesDailyReturn.index.name = 'dtEnd'
    
    ### output for check
    """
    excel_writer = pd.ExcelWriter(dictPerTopPort['dirCasePort'] + 'Check.xlsx')
    Utils.funcWriteExcel(dfVolatility, excel_writer, 'Volatility')
    Utils.funcWriteExcel(dfReturnValid, excel_writer, 'ReturnValid')
    Utils.funcWriteExcel(dfWeightValid * dfIndicator, excel_writer, 'WeightDirection')
    Utils.funcWriteExcel(dfPositionDirection, excel_writer, 'PositionDirection')
    Utils.funcWriteExcel(pd.DataFrame(seriesDTRebalance), excel_writer, 'DTRebalance')
    Utils.funcWriteExcel(pd.DataFrame(seriesDailyReturn), excel_writer, 'DailyReturn')
    Utils.funcWriteExcel(pd.DataFrame(sEquityPCT), excel_writer, 'sEquityPCT')
    Utils.funcWriteExcel(dfEquityDeltaDaily, excel_writer, 'EquityDeltaDaily')

    if boolStoploss:
        Utils.funcWriteExcel(pd.concat(listSeriesStoploss, axis=1), excel_writer, 'Stoploss')
        Utils.funcWriteExcel(pd.concat(listSeriesReturnStoploss, axis=1), excel_writer, 'ReturnStoploss')
        Utils.funcWriteExcel(pd.concat(listStoplossPrice, axis=1), excel_writer, 'StoplossPrice')
        Utils.funcWriteExcel(pd.concat(listStoplossPriceTomorrow, axis=1), excel_writer, 'StoplossPriceTomorrow')

    # freeze panes
    for sheet in excel_writer.sheets:
        excel_writer.sheets[sheet].freeze_panes(1, 1)
    excel_writer.close()
    """
    # dfOut
    seriesDailyValue = (1 + seriesDailyReturn).cumprod()
    seriesDailyValue.name = 'Cum Return'
    seriesMaxValue = seriesDailyValue.expanding().max()
    seriesMaxDD = (seriesMaxValue - seriesDailyValue) / seriesMaxValue
    seriesMaxDD.name = 'Max DD'
    dfOut = pd.concat([seriesDailyValue-1, seriesMaxDD, seriesDailyPosition], axis=1)

    return dfOut

############################################################
# Top
############################################################
def funcTopPort(strParamSweep):
    # dtBackTestStart
    dtBackTestStart = PARAMS.dtBackTestStart

    ######################################################### 
    # read backtest result of rquired cases
    ######################################################### 
    # read stored results
    listDictPerTopPort = Utils.getFileAddressForTopPort(strParamSweep)
        
    ######################################################### 
    # prepare directory for saving
    ######################################################### 
    #-- weight param set
    dictWeightParamSet = getDictWeightParamSet(strParamSweep)
    #---------------- iterate 
    dictDictChunk = {}
    for nSpec, dictPerTopPort in enumerate(listDictPerTopPort):
        dictPerTopPort['strParamSweep'] = strParamSweep
        dictPerTopPort.update(dictWeightParamSet)
        dictDictChunk[nSpec] = dictPerTopPort
        API.funcPerTaskSync(dictPerTopPort)

    ########################################################
    # submit jobs
    ########################################################
    import ThaiExpress.Common.Parallel as Parallel
    reload(Parallel)
    dictOptionDefault = {
            'ncpus': PARAMS.ncpus,
            'funcTask': API.funcPerTaskSync,
            }
    #Parallel.Utils.submitjobs(dictDictChunk, dictOptionDefault)

if __name__ == '__main__':
    strParamSweep = sys.argv[1]
    dfOut = funcTopPort(strParamSweep)
