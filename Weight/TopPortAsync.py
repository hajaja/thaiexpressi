#coding=utf8
import pandas as pd
import datetime
import numpy as np
import os, re, sys, shutil, logging

import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS)
import UtilsPortAsync
reload(UtilsPortAsync)

def funcTopPort(strParamSweep):
    # dtBackTestStart & dtBackTestEnd
    dtBackTestStart = PARAMS.dtBackTestStart

    ######################################################### 
    # read backtest result of rquired cases
    ######################################################### 
    # read stored results
    listDictPerTopPort = Utils.getFileAddressForTopPort(strParamSweep)
    
    # read dfPCTAll, dfVolatilityAll
    print('reading data')
    #dictDataSpec = Utils.extractElementFromList(listDictPerTopPort[0])
    dictDataSpec = dict(listDictPerTopPort[0])
    listSPCT = []
    listVolatility = []
    for SecuCode in dictDataSpec['Secu']:
        dictDataSpec['Secu'] = SecuCode
        dictDataSpec['freq'] = '1day'
        dfAll = Utils.getTradingDataPoint_Commodity(dictDataSpec).replace(np.inf, np.nan)
        dtLastObservation = dfAll.index[-1]
        dtEnter = PARAMS.dtEnter
        rowLast = dfAll.ix[dtLastObservation]
        rowLast.name = dtEnter
        dfAll = dfAll.append(rowLast)
        dictDataSpec['df'] = dfAll

        sPCT = dfAll['Close'].pct_change().dropna()
        sPCT.name = SecuCode
        listSPCT.append(sPCT)
        
        sValueDaily = dfAll['Close'].copy()
        sVolatility = sValueDaily.pct_change().rolling(60).std().shift(1) * np.sqrt(PARAMS.NTradingDayPerYear)
        sVolatility.name = SecuCode
        listVolatility.append(sVolatility)

    dfPCTAll = pd.concat(listSPCT, axis=1)
    dfVolatilityAll = pd.concat(listVolatility, axis=1)

    print('iterate listDictPerTopPort')
    for dictPerTopPort in listDictPerTopPort:
        print(dictPerTopPort)
        ######################################################### 
        # prepare directory for saving
        ######################################################### 
        strFileAddressPrefix = dictPerTopPort['dirCasePort']

        #-- save config file of the port
        dictPerTopPortOut = {}
        for key, value in dictPerTopPort.items():
            if key not in ['strCase', 'listFileAddress', 'Secu']:
                if type(value) is not list:
                    dictPerTopPortOut[key] = value
                else:
                    dictPerTopPortOut[key] = value[0]
        strFileAddressPortConfig = '%s/portconfig.pickle'%dictPerTopPort['dirCasePort']
        pd.Series(dictPerTopPortOut).to_pickle(strFileAddressPortConfig)

        #-- save config file of the port
        listDict = []
        dictDTEnter = {}
        dictIndicator = {}
        listSecuCode = dictPerTopPort['Secu']
        listFileAddress = dictPerTopPort['listFileAddress']
        for nSecuCode, strFileAddress in enumerate(listFileAddress):
            SecuCode = listSecuCode[nSecuCode]
            sPCT = dfPCTAll[SecuCode]
            sVolatility = dfVolatilityAll[SecuCode]
            sVolatility.name = SecuCode

            # dictOne
            dictOne = dict(dictPerTopPort)
            del dictOne['strCase']
            del dictOne['Secu']
            del dictOne['listFileAddress']
            
            ######################################################### 
            # calculate performance for each Secu
            ######################################################### 
            # read indicator
            strategy = pd.read_pickle(strFileAddress)['strategy']

            sIndicator = strategy.df['indicator']
            dictIndicator[SecuCode] = sIndicator
            
            # calculate PCT daily, compatible with 1min back test
            sPCTHold = sPCT * sIndicator
            sPCTHold.name = SecuCode
            sValue = (1 + sPCTHold).cumprod()
            sValueDaily = sValue
            sPCTDaily = sValueDaily.pct_change()

            dictMetric = Utils.funcMetric(sPCTDaily)
            dictOne.update(dictMetric)
            sPCTDaily.name = SecuCode

            # find index Enter to 1) calculate trading cost 2) determine position when entering
            sIndexBoth = strategy.indexLongEnter.append(strategy.indexShortEnter)
            listIndexEnter = set(sIndexBoth.date).intersection(set(sPCTDaily.dropna().index.date.tolist()))
            listIndexEnter = np.sort(list(listIndexEnter))
            dictDTEnter[SecuCode] = listIndexEnter

            sPCTDaily.ix[listIndexEnter] = sPCTDaily.ix[listIndexEnter] - PARAMS.COMMISSION * 2
            dictOne['sPCTDaily'] = sPCTDaily
            
            # determine position
            # here, the lambda x: 0.005/max(0.001, x) is to make sure any single product does not dominant the portfolio value
            sPosition = sVolatility.apply(lambda x: np.nan)
            sPosition.ix[listIndexEnter] = sVolatility.ix[listIndexEnter].apply(lambda x: 0.1/max(0.05,x))
            sPosition = sPosition.sort_index()
            sPosition = sPosition.ffill()
            sPCTDaily = sPCTDaily * sPosition
            sPosition.name = SecuCode
    
            # no position when sIndicator=0
            dictOne['sPosition'] = sPosition
            dictOne['sVolatility'] = sVolatility
            dictOne['listIndexEnter'] = listIndexEnter

            # 
            listDict.append(dictOne)
        
        # below are the dataframe for all products in a case
        dfResult = pd.DataFrame(listDict)
        dfPCT = pd.concat(dfResult['sPCTDaily'].values.tolist(), axis=1)
        dfPosition = pd.concat(dfResult['sPosition'].values.tolist(), axis=1)
        
        ######################################################### 
        # adjust the position according to the number of all products available
        ######################################################### 
        sNSecuRaw = Utils.generateNSecu()
        sNSecu = sNSecuRaw
        sNSecu = sNSecuRaw.apply(lambda x: np.sqrt(x))

        # Faith Position
        for strSecu in dfPosition.columns:
            dfPosition[strSecu] = dfPosition[strSecu] / sNSecu
            pass
        seriesDailyPosition = dfPosition.sum(1)
        seriesDailyPosition.name = 'Position'
        
        ######################################################### 
        # calculate number of contracts for a product
        ######################################################### 
        # if some secu's weight is smaller than one contract, then do not trade
        TotalMoney = PARAMS.TOTALMONEY 
        dfPositionDollar = dfPosition * TotalMoney
        listPositionContract = []
        listPositionDirection = []
        listPositionDecimal = []
        for SecuCode in dfPositionDollar.columns:
            sPositionDollar = dfPositionDollar[SecuCode]

            #sSettleRaw = Utils.dfExe.ix[SecuCode]['SettleRaw'].shift(1).ix[dictDTEnter[SecuCode]]
            sSettleRaw = Utils.getDFOneProduct(SecuCode, ['SettleRaw']).shift(1).ix[dictDTEnter[SecuCode]]
            sSettleRaw.name = SecuCode
            sNominalContract = sSettleRaw * Utils.dfDetail.ix[SecuCode, 'multiplier']

            sPositionContract = sPositionDollar / sNominalContract.ix[sPositionDollar.index].ffill()
            sPositionContract = sPositionContract.apply(lambda x: np.round(x))
            sPositionContract.name = SecuCode
            listPositionContract.append(sPositionContract)

            sIndicator = dictIndicator[SecuCode]
            sPositionDirection = sPositionContract * sIndicator.ix[sPositionContract.index].ffill()
            sPositionDirection.name = SecuCode
            listPositionDirection.append(sPositionDirection)

            sPositionDecimal = sPositionContract * sNominalContract / TotalMoney
            sPositionDecimal = sPositionDecimal * sIndicator.ix[sPositionDecimal.index].ffill()
            sPositionDecimal = sPositionDecimal.ffill()
            sPositionDecimal.name = SecuCode
            listPositionDecimal.append(sPositionDecimal)

        dfPositionContract = pd.concat(listPositionContract, axis=1)
        dfPositionDirection = pd.concat(listPositionDirection, axis=1)
        dfWeightDirection = pd.concat(listPositionDecimal, axis=1)
        
        # calculate daily return as in WH
        listEquityDeltaDaily = []
        for SecuCode in dfPositionDirection.columns:
            listColumn = ['OpenRaw', 'CloseRaw', 'SettleRaw', 'DeltaSettle', 'Open-PreSettle', 'Settle-Open']
            dfTrading = Utils.getDFOneProduct(SecuCode, listColumn)
            dfTrading = dfTrading.rename({'OpenRaw':'Open', 'CloseRaw':'Close', 'SettleRaw':'Settle'})

            # contracts in and out
            sPositionDirection = dfPositionDirection[SecuCode]
            sPositionDirectionDiff = sPositionDirection.diff()
            sPositionIn = sPositionDirectionDiff.copy()
            sPositionOut = -sPositionDirectionDiff.copy()
            sPositionIn.ix[sPositionDirection.abs() < sPositionDirection.shift(1).abs()] = 0
            sPositionOut.ix[sPositionDirection.abs() > sPositionDirection.shift(1).abs()]  = 0

            # calculate daily equity delta
            sEquity = sPositionDirection * dfTrading['DeltaSettle'] - sPositionIn * dfTrading['Open-PreSettle'] + sPositionOut * dfTrading['Open-PreSettle']
            sEquity = sEquity * Utils.dfDetail.ix[SecuCode, 'multiplier']
            sEquity.name = SecuCode
            listEquityDeltaDaily.append(sEquity)
        dfEquityDeltaDaily = pd.concat(listEquityDeltaDaily, 1)
        dfEquityDeltaDaily['Total'] = dfEquityDeltaDaily.sum(1)

        # calculate daily equity PCT
        sEquityPCT = dfEquityDeltaDaily['Total'] / PARAMS.TOTALMONEY
        
        ######################################################### 
        # output
        ######################################################### 
        #'''
        # daily value & DD
        seriesDailyReturn = sEquityPCT
        sDailyReturn = sEquityPCT
        sDailyReturn = sDailyReturn[sDailyReturn.index >= dtBackTestStart]
        seriesDailyPosition = seriesDailyPosition[seriesDailyPosition.index >= dtBackTestStart]
        seriesDailyValue = (1 + sDailyReturn).cumprod()
        seriesDailyValue.name = 'Cum Return'
        seriesMaxValue = seriesDailyValue.expanding().max()
        seriesMaxDD = (seriesMaxValue - seriesDailyValue) / seriesMaxValue
        seriesMaxDD.name = 'Max DD'
        dfOut = pd.concat([seriesDailyValue-1, seriesMaxDD, seriesDailyPosition], axis=1)
        
        # plot & savefig
        dfOut = dfOut[['Cum Return', 'Max DD', 'Position']].ffill().dropna()
        if dfOut.empty:
            print('empty dfOut')
            shutil.rmtree(strFileAddressPrefix)
            continue
        dfOut.to_pickle(strFileAddressPrefix + 'dfOut.pickle')

        # check
        strFileAddress = strFileAddressPrefix + '/Check.xlsx'
        excel_writer = pd.ExcelWriter(strFileAddress)
        Utils.funcWriteExcel(dfPositionDirection, excel_writer, 'PositionDirection')
        Utils.funcWriteExcel(dfEquityDeltaDaily, excel_writer, 'EquityDeltaDaily')
        #Utils.funcWriteExcel(dfReturnValid, excel_writer, 'ReturnValid')
        Utils.funcWriteExcel(dfWeightDirection, excel_writer, 'WeightDirection')
        #Utils.funcWriteExcel(dfDailyReturnCheck, excel_writer, 'DailyReturn')

        dfContractCode = Utils.getTableExe('ContractCode')
        Utils.funcWriteExcel(dfContractCode, excel_writer, sheet_name='ContractCode')
        
        excel_writer.close()
        #'''

if __name__ == '__main__':
    strParamSweep = sys.argv[1]
    funcTopPort(strParamSweep)
