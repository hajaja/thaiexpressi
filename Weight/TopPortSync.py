# -*- coding:utf8 -*-
######################################################### 
'''
The weight of a product's position is determined by its standard deviation's reciprocal. More academic resources can be found in[Qian2005](http://www.dailyalts.com/wp-content/uploads/2014/06/Risk-Parity-and-Diversification.pdf)

For better cash management, the total weight is always constant, and the individual risk is used to determine the relative importance in the portfolio. 

Future works
1. the lookback period for calculating the product's risk
2. the method to calculate the product's risk. a) standard deviation; b) exponential weighted moving average of standard deviation; c) other risk measures. 
3. how to determine the total weight of the portfolio. If all the products are volatile, then holding more cash is a better option. 
'''
######################################################### 
import pandas as pd
import datetime, os, re, sys, time, logging, gc
import numpy as np
from imp import reload

import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS)
import ThaiExpress.Weight.UtilsPortSync as UtilsPortSync
reload(UtilsPortSync)

if __name__ == '__main__':
    strParamSweep = sys.argv[1]
    dfOut = UtilsPortSync.funcTopPort(strParamSweep)

