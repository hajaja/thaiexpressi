# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
import pdb, datetime, os, shutil, random, time, gc, re, sys
from dateutil.parser import parse
from imp import reload
import ThaiExpress.Common.Utils as Utils
reload(Utils)
import ThaiExpress.Common.PARAMS as PARAMS
reload(PARAMS)

def Sweep():
    #-- prep data
    import ThaiExpress.Prepdata.concat as concat
    reload(concat)
    import ThaiExpress.Prepdata.DataAnalysis as DataAnalysis
    reload(DataAnalysis)
    concat.processData()

    #-- run TSM_Classic, TSM_Small
    import ThaiExpress.Config.TSM.TopTSM as TopTSM
    reload(TopTSM)
    TopTSM.top('TSM_Classic')
    TopTSM.top('TSM_SectorLimit')
    TopTSM.top('TSM_SensitivityReduction')
    TopTSM.top('TSM_Small')
    TopTSM.top('TSM_NthFriday')

    #-- run MFM
    if PARAMS.boolCplexAvailable:
        import ThaiExpress.Config.MFM.TopMFM as TopMFM
        reload(TopMFM)
        TopMFM.top('ReturnMax')
        TopMFM.top('VarianceMin')

    #-- run plot
    Plot()

    return

def CopyToAsync():
    from shutil import copyfile
    import ThaiExpress.Common.TexPlot as TexPlot
    reload(TexPlot)
    # param
    strMetric = 'SR'

    # Select the best (K,H) for each SecuCode
    dictDataSpec = dict(PARAMS.dictDataSpecTemplate)
    dictDataSpec['strModelName'] = 'TSM'
    dictDataSpec['freq'] = '1day'
    listDictDataSpecTarget = Utils.sweepParam(dictDataSpec, 'TSM_Async')
    listDictDataSpecSource = Utils.sweepParam(dictDataSpec, 'TSM_Small')
    listDictDataSpecSource = Utils.sprawnDictPerCase(listDictDataSpecSource)

    # Sharpe ratio per Secu
    if strMetric == 'CR':
        strPickleAddress = PARAMS.strFileAddressKHCRPerSecu
    elif strMetric == 'SR':
        strPickleAddress = PARAMS.strFileAddressKHSRPerSecu

    if os.path.exists(strPickleAddress):
        dfMetric = pd.read_pickle(strPickleAddress)
    else:
        dfMetric_3_1 = TexPlot.texPlotMetricPerSecu(NMonthLookBack=3, NMonthHolding=1)
        dfMetric_3_2 = TexPlot.texPlotMetricPerSecu(NMonthLookBack=3, NMonthHolding=2)
        dfMetric_12_1 = TexPlot.texPlotMetricPerSecu(NMonthLookBack=12, NMonthHolding=1)
        dfMetric_12_2 = TexPlot.texPlotMetricPerSecu(NMonthLookBack=12, NMonthHolding=2)
        dfMetric = pd.concat([dfMetric_3_1[strMetric], dfMetric_3_2[strMetric], dfMetric_12_1[strMetric], dfMetric_12_2[strMetric]], axis=1)
        dfMetric.columns = ['3,1', '3,2', '12,1', '12,2']
        dfMetric.to_pickle(strPickleAddress)

    # which KH is the best
    dfMetric['KH'] = np.nan
    listKH = dfMetric.columns
    dictProductKH = {}
    for ix, row in dfMetric.iterrows():
        for KH in listKH:
            if row.max() == row[KH]:
                dictProductKH[ix] = KH
    sProductKH = pd.Series(dictProductKH)

    # copy signal to async folder
    dfDataSpec = pd.DataFrame(listDictDataSpecSource).set_index(['NMonthTrain', 'NMonthTest','Secu']).sort_index()
    dfDataSpec = dfDataSpec[(dfDataSpec['NMonthStart']==0)&(dfDataSpec['NDayShift']==0)]
    dirTarget = listDictDataSpecTarget[0]['dirCaseSignal']
    for SecuCode,KH in sProductKH.items():
        NMonthLookBack, NMonthHolding = re.compile('(\d+),(\d+)').match(KH).groups()
        NMonthLookBack = int(NMonthLookBack)
        NMonthHolding = int(NMonthHolding)
        strFileAddressSource = dfDataSpec.loc[(NMonthLookBack,NMonthHolding,SecuCode), 'strFileAddress']
        strFileAddressTarget = '%s/%s.pickle'%(dirTarget, SecuCode)
        
        # copy
        copyfile(strFileAddressSource, strFileAddressTarget)
        
    return

def Plot():
    import ThaiExpress.Common.TexPlot as TexPlot
    reload(TexPlot)

    # summary & correlation
    import ThaiExpress.Prepdata.DataAnalysis as DataAnalysis
    reload(DataAnalysis)
    dfSummaryStatistics = DataAnalysis.prepSummaryStatistics()
    dfEWIndex = DataAnalysis.prepCommodityMarketFactor()
    dfCorrelation = DataAnalysis.prepCorrelationMatrix(dfEWIndex)

    # (K, H) sweep
    dfMetric,sV = Utils.funcCalcPortKH('TSM_Small')
    dfTable = TexPlot.texPlotKHTable(dfMetric, 'SR')

    # plot best (K, H) daily value
    TexPlot.texPlotStrategyPerf(sV, PARAMS.strFigureAddressStrategyPerf0)

    # sensitivity
    dfMetricBest,sVClassic = Utils.funcCalcPort('TSM_Classic')
    TexPlot.texPlotSensitivityOnMarket(sVClassic, AddressID=0)
    dfMetricSensitivity,sVSensitivity = Utils.funcCalcPort('TSM_SensitivityReduction')
    TexPlot.texPlotSensitivityOnMarket(sVSensitivity, AddressID=2)

    # Sharpe ratio per Secu
    dfMetric_3_1 = TexPlot.texPlotMetricPerSecu(NMonthLookBack=3, NMonthHolding=1)
    dfMetric_3_2 = TexPlot.texPlotMetricPerSecu(NMonthLookBack=3, NMonthHolding=2)
    dfMetric_12_1 = TexPlot.texPlotMetricPerSecu(NMonthLookBack=12, NMonthHolding=1)
    dfMetric_12_2 = TexPlot.texPlotMetricPerSecu(NMonthLookBack=12, NMonthHolding=2)
    dfSR = pd.concat([dfMetric_3_1['SR'], dfMetric_3_2['SR'], dfMetric_12_1['SR'], dfMetric_12_2['SR']], axis=1)
    dfSR.columns = ['3,1', '3,2', '12,1', '12,2']
    dfSR.round(2).astype(str).to_latex(PARAMS.strTableAddressKHSRPerSecu)

    # asynchronous port
    #dfMetricAsync,sVAsync = Utils.funcCalcPort('TSM_Async')
    #strFigureAddress = PARAMS.strFigureAddressValueComparisonTemplate.format('Async')
    #TexPlot.texPlotComparison(sVClassic, sVAsync, ['Classic','Async'], strFigureAddress)

    # define sector 
    dfMetricSectorLimit,sVSectorLimit = Utils.funcCalcPort('TSM_SectorLimit')
    strFigureAddress = PARAMS.strFigureAddressValueComparisonTemplate.format('SectorLimit')
    TexPlot.texPlotComparison(sVClassic, sVSectorLimit, ['Classic','SectorLimit'], strFigureAddress)
    dfTableComparison = TexPlot.prepareDfTable_DefineSector()
    TexPlot.texTableDumpComparison(dfTableComparison, PARAMS.strTableAddressSectorLimit)

    # Multi-Factor Model
    if PARAMS.boolCplexAvailable:
        dfTableComparison = TexPlot.prepareDfTable_MFM()
        TexPlot.texTableDumpComparison(dfTableComparison, PARAMS.strTableAddressMFM)

    return
    

if __name__ == '__main__':
    Option = sys.argv[1]
    if Option == 'Sweep':
        Sweep()
    elif Option == 'Plot':
        Plot()
    elif Option == 'CopyToAsync':
        CopyToAsync()

