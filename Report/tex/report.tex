% multiline.tex - Example file illustrating why the eqnarray package is
%                 useful, and how to use it.
%
% Andrew Roberts - 3rd August 2004
% 
% 19th April 2011 - Replaced incorrect reference to "equarray" with "eqnaray".

\documentclass[a4paper]{article}   % article does not support \section
%\documentclass[utf-8]{report}

% The mathptmx package does for maths equations what the times package
% does for the main text. That is, uses scalable fonts rather than the
% default bitmapped ones. This is useful if you later want to convert
% your postscript file to PDF.

% reference

% input to_latex file
\usepackage{booktabs}

% link
\usepackage{hyperref}

% \mathbb
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{mathptmx}

% unknown
\usepackage{array}
\usepackage{url}

% header and footer
\usepackage{fancyhdr}
\pagestyle{fancy}

% graph
\usepackage{graphicx}
\usepackage{subcaption}

\begin{document}
\title{Report on Time Series Momentum Strategy}
\author{\\
Chuansheng Dong\\
\url{dcsfudan@gmail.com}}
\date{\today}
\maketitle

\newpage
\abstract{
    Trend following strategy is explored in this work, with focus on commodity market between 1970 and 2012. The classic 12 months lookback period and 1 month holding period strategy is backtested along with other lookback\&holding pairs, with Sharpe ratio of 0.84. 
    To improve the strategy some methods are investigated, such as limit on single direction exposure, limit on single sector exposure, week pattern, and multi-factor models. The Sharpe ratio of the strategy is improved to 0.96. 
}

\newpage
\tableofcontents

\newpage
\section{Introduction}\label{sec:Introduction}
Trend following strategy may be the most robust strategy of all time. The performance can be verified by long-span data sets~\cite{hurst2017century,chabot2009momentum}. This phenomenon can be partly explained by two paradoxical reasons: under reaction (at trend start) and over reacation (at trend end)~\cite{moskowitz2012time}. Many different strategies popular among investors are ``trend following'' in nature. The only difference may lie on the method of recognizing it. The simple ``Two Newspaper''(according to Cliff Asness in one interview) method may be the simplest one. 

\section{Data Analysis}\label{sec:DataAnalysis}
\subsection{Summary Statistics}\label{sec:SummaryStatistics}
The summary statistics are exhibited in Table~\ref{tab:SummaryStatistics}. 
\begin{table}[!htb]
    {\footnotesize
    \begin{center}
        \caption{Summary Statistics}
        \label{tab:SummaryStatistics}
        \input{./tables/SummaryStatistics.txt}
    \end{center}
    }
\end{table}

Among all the contracts, Natural Gas has a unique First Observation of 10000, probably due to the extreme negative roll yield in past years. Natural Gas is not the only contract suffering from the negative roll yield. Agricultural contracts like Corn, Cotton, Sugar, Cocoa, and some metal contracts exhibit similar contango structures, due to the motivation to lock the uncertainty in the future. 

Except for Natural Gas, all the other contracts have the same First Observation 100. This is because the data sets given are index of the contracts instead of the raw price. The generation of index can help consider the roll yield when rolling over positions in the backtest. However, the trend generated from index data may be different with that from spot price data.

\subsection{Equally Weighted Index}\label{sec:EquallyWeightedIndex}
\begin{figure}[!htb]
    \includegraphics[width=\linewidth]{./figures/EWIndex.eps}
    \caption{Equally Weighted Index}
    \label{fig:EWIndex}
\end{figure}
The market as whole exhibits trend itself, so a market index is necessary to measure it. In Figure~\ref{fig:EWIndex}, the equally weighted index is shown. One minor defect of this index is that it is daily rebalancing, different from the main stream index convention.

\subsection{Correlation}\label{sec:Correlation}
\begin{figure}[!htb]
    \includegraphics[width=\linewidth]{./figures/CorrelationMatrix.eps}
    \caption{Correlation Matrix}
    \label{fig:CorrelationMatrix}
\end{figure}
In Figure~\ref{fig:CorrelationMatrix}, the correlation matrix is exhibited, with dark color as weak correlation and white as strong correlation. The diagonal blocks represent for the autocorrelation, and they are all white. 

\subsection{Difference with Reference}\label{sec:DifferenceWithReference}
It should be noted that compared with the data summary statistics in the work~\cite{moskowitz2012time}, the data sets given have different first trading day, and different annual mean return. This may be caused by different data processing on the raw trading data.

\section{Time Series Momentum Strategy}\label{sec:TSM}
\subsection{Risk Adjusted Position}\label{sec:RiskAdjustedPosition}
Using the inverse of volatility as the weight of the corresponding contract, the portfolio is less exposed to the riskier assets. 

The proxies for the volatility include standard deviation, exponentially weighted standard deviation, volatility using intra-day trading data~\cite{bennett2012measuring}, or other more advanced models like GARCH. In this work, the simple standard deviation of the past 60 days daily return is used as the proxy for volatility. 

\subsection{Look Back Period \& Holding Period}\label{sec:LookBackAndHolding}
\begin{table}[!htb]
    {\footnotesize
    \begin{center}
        \caption{Sharpe Ratio of Time Series Momentum Strategies}
        \label{tab:KHSR}
        \input{./tables/KHSR.txt}
    \end{center}
    }
\end{table}

\begin{equation}\label{eqn:MOM}
    \operatorname{MOM_K} = \frac{p_{t}}{p_{t-K}} - 1
\end{equation}
\begin{equation}\label{eqn:Ret}
    \operatorname{Ret_H} = \frac{p_{t+H}}{p_{t}} - 1
\end{equation}

Lookback period (K) is used to define momentum as in (\ref{eqn:MOM}). 
Once a position is entered, it is kept for Holding period (H), which has the return as defined in (\ref{eqn:Ret})

\subsection{Performance of Strategy}\label{sec:PerformanceOfStrategy}
The performance of the strategy is shown in Figure~\ref{fig:StrategyPerf0}.
\begin{figure}[!htb]
    \includegraphics[width=\linewidth]{./figures/StrategyPerf0.eps}
    \caption{Time Series Momentum Strategy Performance, K=12, H=1}
    \label{fig:StrategyPerf0}
\end{figure}
In the backtest results of this report, the leverage is set to 1.0, at each rebalancing date, this is to remove the potential affects from market volatility. The only exception is the asynchronous strategy discussed in Section~\ref{sec:AsynchronousRebalancing}, which is not limited on leverage.

\subsection{Sensitivity with Commodity Market}\label{sec:SenstivityWithCommodityMarket}
\begin{figure}[!htb]
    \includegraphics[width=\linewidth]{./figures/TSMSmile0.eps}
    \caption{Time Series Momentum Smile (quarterly return)}
    \label{fig:TSMSmile0}
\end{figure}

Similar with the work~\cite{moskowitz2012time}, the Time Series Momentum smile is shown in Figure~\ref{fig:TSMSmile0}. As can be seen in the figure, the strategy exhibits better performance when the market is more volatile (in either direction). 

The Time Series Momentum strategy is long-short instead of long only (like the equally weighted index). So the performance of the strategy is dependent on swing amplitude of the equally weighted index. When we run the regression of the strategy monthly return on that of the absolute of the monthly return of the equally weighted index, we can have a 0.24 correlation. 

To reduce the exposure to the market risk, the total position on one direction (long or short) is limited. This means we cannot have all contracts in the long direction or short direction. The difference between the total long position and total short position has an upper limit of 50\% of the principal. After adding this limit, the sensitiviy is reduced, as shown in Figure~\ref{fig:Sensitivity2}. At the same time, the Sharpe ratio is also increased from 0.88 to 0.91. 

\begin{figure}[!htb]
    \begin{subfigure}[b]{0.9\textwidth}
        \includegraphics[width=\textwidth]{./figures/Sensitivity0.eps}
        \caption{Sensitivity without one-direction total position constraint}
        \label{fig:Sensitivity0}
    \end{subfigure}

    \begin{subfigure}[b]{0.9\textwidth}
        \includegraphics[width=\textwidth]{./figures/Sensitivity2.eps}
        \caption{Reducing sensitivity by limiting one-direction total position}
        \label{fig:Sensitivity2}
    \end{subfigure}
    \caption{Sensitivity of TimeSeries Momentum Strategy Performance on Equally Weighted Market Index}
\end{figure}

\subsection{Contribution of Each Sector}\label{sec:ContributionOfEachSector}
\begin{figure}[!htb]
    \includegraphics[width=\linewidth]{./figures/ContribPerProduct.eps}
    \caption{Contribution by Product}
    \label{fig:ContribPerProduct}
\end{figure}
\begin{figure}[!htb]
    \includegraphics[width=\linewidth]{./figures/ContribPerSector.eps}
    \caption{Contribution by Sector}
    \label{fig:ContribPerSector}
\end{figure}

The measure of contribution of each sector is dependent on the method of back test. 

Suppose Corn is only traded once in 1990 (profit is 100 thousand million when the total principal is 1 million), and Cattle is also traded once but in 2010 (profit is 2 million when fund capital is 20 million). In terms of return rate, it is the same, but in terms of dollar amounts, Cattle trade is more profitable. 

To overcome this issue caused by different fund capital, we fix the total principal (2 million dollar) at each rebalancing date, and calculate the money earned in dollar, as shown in Figure~\ref{fig:ContribPerSector} and Figure~\ref{fig:ContribPerProduct}. 

\section{Improvements \& Future Works}\label{sec:Improvements}
\subsection{Asynchronous Rebalancing}\label{sec:AsynchronousRebalancing}
\begin{table}[!htb]
    {\footnotesize
    \begin{center}
        \caption{Sharpe ratio of different Lookback\&Holding period }
        \label{tab:KHSRPerSecu}
        \input{./tables/KHSRPerSecu.txt}
    \end{center}
    }
\end{table}
The Sharpe ratio on different Lookback period and Holding period shown in Table~\ref{tab:KHSR} are for all the contracts as a whole, and the Lookback\&Holding pair that is best may be less suitable for individual contracts. For example, Corn has a very different supply-demand system when compared with LME Zinc, and the trend of these two contracts are also different. 

%To investigate into the specific trend pattern of different contracts, we tested their performance in different (LookBack, Holding) pair, as shown in Figure~\ref{fig:MetricPerSecu_12_1}, Figure~\ref{fig:MetricPerSecu_3_1}, Figure~\ref{fig:MetricPerSecu_12_2}, and Figure~\ref{fig:MetricPerSecu_3_2}. It should be noted that, these 4 pairs are not necessary covering all the trend patterns, further tests are needed. These 4 pairs are shown as an example of potential improvements. A summary of Sharpe ratio is shown in Table~\ref{tab:KHSRPerSecu}

One portfolio constructed by using different Lookback\&Holding period is shown in Figure~\ref{fig:ValueComparison_Async}. However, the Sharpe ratio is not improved. Future works may include investigation into the logics behind different optimal lookback\&holding period for different products.

\iffalse
\begin{figure}[ht]
    \includegraphics[width=\linewidth]{./figures/MetricPerSecu_12_1.eps}
    \caption{Sharpe ratio of Time Series Trend Strategy, K=12,H=1}
    \label{fig:MetricPerSecu_12_1}
\end{figure}

\begin{figure}[ht]
    \includegraphics[width=\linewidth]{./figures/MetricPerSecu_3_1.eps}
    \caption{Sharpe ratio of Time Series Trend Strategy, K=3,H=1}
    \label{fig:MetricPerSecu_3_1}
\end{figure}

\begin{figure}[ht]
    \includegraphics[width=\linewidth]{./figures/MetricPerSecu_12_2.eps}
    \caption{Sharpe ratio of Time Series Trend Strategy, K=12,H=2}
    \label{fig:MetricPerSecu_12_2}
\end{figure}

\begin{figure}[ht]
    \includegraphics[width=\linewidth]{./figures/MetricPerSecu_3_2.eps}
    \caption{Sharpe ratio of Time Series Trend Strategy, K=3,H=2}
    \label{fig:MetricPerSecu_3_2}
\end{figure}
\fi

\begin{figure}[!htb]
    \includegraphics[width=\linewidth]{./figures/ValueComparison_Async.eps}
    \caption{Classic (12,1) and Asynchronous Rebalancing}
    \label{fig:ValueComparison_Async}
\end{figure}

\subsection{Define Sector based on Correlation}\label{sec:DefineSectorBasedOnCorrelation}
There are three sectors: Agri\&Cattle, Energy, and Metal. As shown in the correlation matrix~\ref{fig:CorrelationMatrix}, contracts from different sectors have low correlation, and the contracts in energy and metal form their own medium size blocks. 

The Agri\&Cattle sector is a little bit different, and can be divided further to several sub-sectors: ``Kansas Wheat'', ``Corn'', ``Soybeans'' as Starch; 
``Feeder Cattle'',``Live Cattle'', ``Lean hogs'' as Cattle. ``Sugar'', ``Cotton'', ``Cocoa'', ``Coffee'' are separate sub-sectors. Another sub-sector from Metal may be ``Gold'' and ``Silver''.

A reasonable sectoring can help reduce exposure to the sector-specific risk. For example, buying ``WTI'' and ``Brent Crude'' is actually not diversified, although they are different products. However, buying Cattle and Corn can have negative correlation. 

\begin{figure}[!htb]
    \includegraphics[width=\linewidth]{./figures/ValueComparison_SectorLimit.eps}
    \caption{Redefine Sector}
    \label{fig:ValueComparison_SectorLimit}
\end{figure}

\begin{table}[!htb]
    {\footnotesize
    \begin{center}
        \caption{Limit Single Sector Exposure}
        \label{tab:SectorLimit}
        \input{./tables/SectorLimit.txt}
    \end{center}
    }
\end{table}

Using redefined sector, the strategy performance is shown in Figure~\ref{fig:ValueComparison_SectorLimit} and Table~\ref{tab:SectorLimit}

\subsection{Portfolio Theory}\label{sec:PortfolioTheory}
\subsubsection{Return Maxmization}\label{sec:MFM}
One limit of the current strategy (as discussed in Section~\ref{sec:TSM}) is that it can consider one Lookback period only. However, different momentumis (1-month momentum, 3-month momentum, 6-month momentum, 12-month momentum) are only proxies of the real time series momentum. 

Another limit is using the inverse volatility as weight, which ignores mutual correlation between contracts. 

Multi-Factor Model~\cite{menchero2008barra} is good at handling the two limits of the strategy discussed in Section~\ref{sec:TSM}. Each momentum can be taken as one factor while mutual correlation can be described by the covariance matrix. 

However, the multi-factor model is not good at return forecasting. The performance of this method is denoted as ``Ret Max'' in Table~\ref{tab:PortfolioTheory}.

\subsubsection{Variance Minimization}\label{sec:VarianceMinimization}
\begin{equation}\label{eqn:formulation}
\begin{array}{l}
\min :\sum\limits_{i = 1}^N {\sum\limits_{j = 1}^N {{w_i}{Cov_{i,j}}{w_j}} }  \\
\text{subject to}: \\
\left\{\begin{array}{l}
    \sum\limits_{i = 1}^{N_{Long}} {{w_{Long,i}}} - \sum\limits_{i = 1}^{N_{Short}} {{w_{Short,i}}} = 1,\;\\
    \forall i = 1,2, \ldots ,N_{Long},\;{w_{Long,i}} \in \{0,1\},\;\\
    \forall i = 1,2, \ldots ,N_{Short},\;{w_{Long,i}} \in \{-1,0\},\;\\
    \end{array} \right. \\
\end{array}
\end{equation}

\begin{table}[!htb]
    {\footnotesize
    \begin{center}
        \caption{Using Portfolio Theory}
        \label{tab:PortfolioTheory}
        \input{./tables/MFM.txt}
    \end{center}
    }
\end{table}

Instead of using the multi-factor model both for return maximization with variance limitation, we can leverage it on variance minimization only, formulated in (\ref{eqn:formulation}). The Sharpe ratio is improved compared with the classic K,H=12,1 time series momentum strategy.

\subsection{Week Pattern}\label{sec:WeekPattern}
The (12,1) case TSM strategy has different Sharpe ratio in Table~\ref{tab:KHSR} and in Table~\ref{tab:PortfolioTheory} or Table~\ref{tab:SectorLimit}. The reason is that In Table~\ref{tab:KHSR}, (12,1) includes 4 possible methods to select trading day in a month (1st Monday, 2nd Monday, 3rd Monday, or Last Monday). In the other two tables, only Last Monday is used. Usually, entering the market in the last week is better than other weeks, and the similar pattern is also valid in China market. 

Except for the week pattern, weekday patter may also deserves further investigation. 

\subsection{Other Potential Improvements}\label{sec:OtherPotentialImprovements}
Higher frequency data (like the 1min data) can be used to determine the trend. For example, average price of the the first 4 hour near the start date and that of the last 4 hour near the end date can help determine the trend as defined in (\ref{eqn:MOM})

Using open interest data. The time when the open interest changes most may be taken as the start of new end.

Other methods of recognizing trend, like the Empirical Mode Decomposition, sudden falling of information entropy extracted from volume or price data.  

There are also other potential directions, which may deviate from the ``trend following'' strategy, such as using more alternative data, alternative models from machine learning community, extracting new factor from high frequency data, etc.

\section{About Code}\label{sec:AboutCode}
This document is generated by Latex. The tex files are in ThaiExpress/Report/tex/report.tex. 
The entering point of the Python3 package ThaiExpress is ThaiExpress/Main.py. You may use ``python Main.py Sweep'' to start. You may need install package openpyxl and xlrd by ``pip install openpyxl xlrd'', or ``conda install openpyxl xlrd''.

\bibliographystyle{abbrv}
\bibliography{ref}

\end{document}


