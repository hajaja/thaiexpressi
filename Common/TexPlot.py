# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
import os, shutil, pdb, datetime, sys, gc, random
from dateutil.parser import parse
from imp import reload

from . import Utils
reload(Utils)
from . import PARAMS
reload(PARAMS)

#########################################
# plot
#########################################
def texPlotKHTable(dfMetric, strMetric):
    strLookback = 'Lookback period (months)'
    strHolding = 'Holding period (months)'
    dictRename = {
        'NMonthLookBack': strLookback, 
        'NMonthHolding': strHolding,
        }
    dfTable = dfMetric[strMetric].reset_index().rename(columns=dictRename).round(2).pivot_table(index=strLookback,columns=strHolding,values=strMetric)
    # dump
    dfTable.to_latex(PARAMS.strTableAddressKHSR)
    return dfTable

def texPlotStrategyPerf(sV, strFigureAddress):
    sDD = sV/sV.expanding().max() - 1
    import matplotlib.pyplot as plt
    # dump

    # lim
    tupleXLim = (sV.index.min(), sV.index.max())
    tupleYLim1 = (0, sV.max() * 1.2)
    tupleYLim2 = (sDD.min() * 1.2, 0)

    # ax1
    fig, ax1 = plt.subplots()
    color = 'tab:red'
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Cumulated Value', color=color)
    ax1.plot(sV.index, sV.values, color=color)
    ax1.set_xlim(tupleXLim)
    ax1.set_ylim(tupleYLim1)
    #ax1.tick_params(axis='y', labelcolor=color)

    # ax2
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:blue'
    ax2.set_ylabel('Draw Down', color=color)  # we already handled the x-label with
    ax2.plot(sDD.index, sDD.values, color=color)
    ax2.set_ylim(tupleYLim2)
    #ax2.tick_params(axis='y', labelcolor=color)

    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.grid()
    plt.savefig(strFigureAddress, format='eps')
    plt.close('all')
    return

def texPlotSensitivityOnMarket(sV, AddressID):
    # concat data
    import matplotlib.pyplot as plt
    import ThaiExpress.Prepdata.DataAnalysis as DataAnalysis
    reload(DataAnalysis)
    sEWPCT = DataAnalysis.prepCommodityMarketFactor()['PCT']
    df = pd.concat([sV.pct_change(), sEWPCT], axis=1).dropna()
    df.columns = ['Port', 'EWIndex']

    # time series momentum smile
    tupleXLim = [-0.25, 0.25]
    tupleYLim = tupleXLim
    dfQ = (1+df).cumprod().resample('1Q').last().pct_change().dropna()
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.set_xlabel('EWIndex returns')
    ax1.set_ylabel('Time Series Momentum returns', color=color)
    ax1.scatter(x=dfQ['EWIndex'], y=dfQ['Port'], color=color)
    ax1.set_xlim(tupleXLim)
    ax1.set_ylim(tupleYLim)

    polyparam = np.polyfit(dfQ['EWIndex'], dfQ['Port'], 2)
    listX = np.linspace(tupleXLim[0], tupleXLim[1], 20)
    listY = []
    for x in listX:
        listY.append(np.polyval(polyparam, x))
    ax1.plot(listX, listY, color='tab:red')
    plt.grid()
    plt.savefig(PARAMS.strFigureAddressTSMSmileTemplate.format(AddressID), format='eps')
    plt.close('all')

    # correlation total
    df = (1+df).cumprod().resample('1M').last().pct_change().dropna()
    corrTotal = df['Port'].corr(df['EWIndex'].abs())
    # correlation per year
    def funcCalcCorr(df):
        return df['Port'].corr(df['EWIndex'].abs())
    sCorr = df.groupby(df.index.year).apply(funcCalcCorr)

    # dump
    tupleXLim = (sCorr.index.min(), sCorr.index.max())
    tupleYLim1 = (-1, 1)
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Correlation Coefficient', color=color)
    ax1.plot(sCorr.index, sCorr.values, color=color)
    ax1.set_xlim(tupleXLim)
    ax1.set_ylim(tupleYLim1)
    strTitle = 'Correlation Coefficient of All Period is %0.2f'%corrTotal
    plt.title(strTitle)
    plt.grid()
    plt.savefig(PARAMS.strFigureAddressSensitivityTemplate.format(AddressID), format='eps')
    plt.close()
    return

def texPlotContribution(strParamSweep='TSM_Classic'):
    dictDataSpec = dict(PARAMS.dictDataSpecTemplate)
    listDictDataSpec = Utils.sweepParam(dictDataSpec, strParamSweep)
    strFileAddressContrib = '%s/%s.pickle'%(listDictDataSpec[0]['dirCasePort'], 'contrib')
    sContrib = pd.read_pickle(strFileAddressContrib).sort_values()
    sContrib = sContrib[sContrib.index!='Total']

    # dump, per product
    import matplotlib.pyplot as plt
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.set_xlabel('Product')
    ax1.set_ylabel('Total Contribution', color=color)
    ax1.bar(sContrib.index, sContrib.values, color=color)
    plt.setp(ax1.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    plt.grid()
    plt.savefig(PARAMS.strFigureAddressContribPerProduct, format='eps')
    plt.close()

    # dump, per sector
    sContribPerProduct = sContrib
    dfContrib = pd.DataFrame(sContrib)
    dfContrib.columns = ['Contrib']
    dfContrib['Sector'] = np.nan
    dfContrib.loc[dfContrib.index.isin(PARAMS.listSecuAgri), 'Sector'] = 'AgriCattle'
    dfContrib.loc[dfContrib.index.isin(PARAMS.listSecuEnergy), 'Sector'] = 'Energy'
    dfContrib.loc[dfContrib.index.isin(PARAMS.listSecuMetal), 'Sector'] = 'Metal'
    sContrib = dfContrib.groupby('Sector')['Contrib'].sum()
    
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    ax1.set_xlabel('Product')
    ax1.set_ylabel('Total Contribution', color=color)
    ax1.bar(sContrib.index, sContrib.values, color=color)
    plt.setp(ax1.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    plt.grid()
    plt.savefig(PARAMS.strFigureAddressContribPerSector, format='eps')
    plt.close()
    
    return

def texPlotMetricPerSecu(NMonthLookBack=12, NMonthHolding=1):
    #-- param
    NDayVolatilityLookBack = 60
    strTypeWeighted = 'Expoential'
    delta = NDayVolatilityLookBack / (NDayVolatilityLookBack+1.)
    volatilityExpected = 0.4

    #-- read data
    df = pd.read_pickle(PARAMS.strFileAddressExe)
    df = df[df.index.get_level_values('TradingDay')<=datetime.datetime(2009,12,31)]
    
    #-- iter SecuCode
    listDictMetric = []
    #listSecuAll = ['Natural gas']
    listSecuAll = PARAMS.listSecuAll
    for SecuCode in listSecuAll:
        sClose = df.loc[SecuCode, 'Close']
        sPCT = sClose.pct_change()
        
        #-- generate listDTMonthEnd
        sDTMonthEnd = sClose.reset_index().groupby(sClose.index.strftime('%Y%m'))['TradingDay'].last()
        listSReturn = []
        arrayWeight = (1-delta) * np.power(delta, range(sPCT.size-1, -1, -1))
        
        for nMonth in range(NMonthLookBack, sDTMonthEnd.size-NMonthHolding, NMonthHolding):
            dtStart = sDTMonthEnd.iloc[nMonth-NMonthLookBack]
            dtEnd = sDTMonthEnd.iloc[nMonth]
            dtFuture = sDTMonthEnd.iloc[nMonth+NMonthHolding]
        
            # direction
            threshold = 0.000
            returnLookBack = sClose.loc[dtEnd]/sClose.loc[dtStart] - 1
            if returnLookBack > threshold:
                indicator = 1.
            elif returnLookBack < -threshold:
                indicator = -1.
            else:
                indicator = 0.
            
            # position
            NPoint = sPCT.loc[:dtEnd].size
            if strTypeWeighted == 'Expoential':
                volatility = (sPCT.loc[:dtEnd]-sPCT.loc[:dtEnd].tail(NDayVolatilityLookBack).mean()).apply(lambda x: x*x) * arrayWeight[-NPoint:]
                volatility = volatility.sum() * PARAMS.NTradingDayPerYear
                volatility = np.sqrt(volatility)
            elif strTypeWeighted == 'Equally':
                volatility = sPCT.loc[:dtEnd].tail(NDayVolatilityLookBack).std() * np.sqrt(PARAMS.NTradingDayPerYear)
            position = volatilityExpected/volatility
        
            # calculate metric
            sReturn = sPCT.loc[dtEnd:dtFuture] * position * indicator
            sReturn = sReturn.iloc[1:]
            listSReturn.append(sReturn)
        
        sReturn = pd.concat(listSReturn, axis=0)
        sV = (1+sReturn).cumprod()
        dictMetric = Utils.funcMetric(sReturn)
        dictMetric['SecuCode'] = SecuCode
        listDictMetric.append(dictMetric)

    #-- plot
    dfMetric = pd.DataFrame(listDictMetric).set_index('SecuCode')
    dfMetric['NMonthLookBack'] = NMonthLookBack
    dfMetric['NMonthHolding'] = NMonthHolding
    sSR = dfMetric['SR']
    
    import matplotlib.pyplot as plt
    fig, ax1 = plt.subplots()
    color = 'tab:blue'
    tupleYLim = (-1., 1.)
    ax1.set_xlabel('Product')
    ax1.set_ylabel('Sharpe ratio', color=color)
    ax1.bar(sSR.index, sSR.values, color=color)
    ax1.set_ylim(tupleYLim)
    plt.setp(ax1.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    plt.grid()
    plt.savefig(PARAMS.strFigureAddressMetricPerSecuTemplate.format(NMonthLookBack, NMonthHolding), format='eps')
    plt.close()
    return dfMetric

def texPlotComparison(sV1, sV2, listLabel, strFigureAddress):
    #-- param
    import matplotlib.pyplot as plt

    #-- concat data
    df = pd.concat([sV1.pct_change(), sV2.pct_change()], axis=1).dropna()
    df.columns = ['1', '2']
    df = (1+df).cumprod()
    sV1 = df['1']
    sV2 = df['2']

    #-- plot
    fig, ax1 = plt.subplots()
    ax1.set_xlabel('Date')
    ax1.set_ylabel('Cumulated Value')
    ax1.plot(sV1.index, sV1.values, color='tab:blue',)
    ax1.plot(sV2.index, sV2.values, color='tab:red',)
    ax1.set_yscale("log")

    ax1.legend(listLabel)
    plt.setp(ax1.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    plt.grid()
    plt.savefig(strFigureAddress, format='eps')
    plt.close()

    return

def prepareDfTable_MFM():
    #-- read data
    sPCTReturnMax = pd.read_pickle(PARAMS.strFileAddressReturnMax)
    sPCTVarianceMin = pd.read_pickle(PARAMS.strFileAddressVarianceMin)
    df,sClassic = Utils.funcCalcPort('TSM_Classic')
    sPCTClassic = sClassic.pct_change()

    listDF = []
    for dtStart in [PARAMS.dtBackTestStart, PARAMS.dtBackTestStartReference]:
        dictReturnMax = Utils.funcMetric(sPCTReturnMax[sPCTReturnMax.index>=dtStart])
        dictVarianceMin = Utils.funcMetric(sPCTVarianceMin[sPCTVarianceMin.index>=dtStart])
        dictClassic = Utils.funcMetric(sPCTClassic[sPCTClassic.index>=dtStart])
        dictReturnMax['Method'] = 'Max Ret'
        dictVarianceMin['Method'] = 'Min Var'
        dictClassic['Method'] = 'K=12,H=1'
        df = pd.DataFrame([dictClassic, dictReturnMax, dictVarianceMin])
        df['Start'] = 'Since' + dtStart.strftime('%Y%m%d')
        listDF.append(df)
    dfTable = pd.concat(listDF).set_index(['Start','Method']).sort_index()
    return dfTable

def prepareDfTable_DefineSector():
    #-- read data
    df,sClassic = Utils.funcCalcPort('TSM_Classic')
    sPCTClassic = sClassic.pct_change()
    dfMetricSectorLimit,sVSectorLimit = Utils.funcCalcPort('TSM_SectorLimit')
    sPCTSectorLimit = sVSectorLimit.pct_change()

    listDF = []
    for dtStart in [PARAMS.dtBackTestStart, PARAMS.dtBackTestStartReference]:
        dictClassic = Utils.funcMetric(sPCTClassic[sPCTClassic.index>=dtStart])
        dictSectorLimit = Utils.funcMetric(sPCTSectorLimit[sPCTSectorLimit.index>=dtStart])
        dictClassic['Method'] = 'K=12,H=1'
        dictSectorLimit['Method'] = 'Limit Sector'
        df = pd.DataFrame([dictClassic, dictSectorLimit])
        df['Start'] = 'Since' + dtStart.strftime('%Y%m%d')
        listDF.append(df)
    dfTable = pd.concat(listDF).set_index(['Start','Method']).sort_index()
    return dfTable

def prepareDfTable_MFM():
    #-- read data
    sPCTReturnMax = pd.read_pickle(PARAMS.strFileAddressReturnMax)
    sPCTVarianceMin = pd.read_pickle(PARAMS.strFileAddressVarianceMin)
    df,sClassic = Utils.funcCalcPort('TSM_Classic')
    sPCTClassic = sClassic.pct_change()

    listDF = []
    for dtStart in [PARAMS.dtBackTestStart, PARAMS.dtBackTestStartReference]:
        dictReturnMax = Utils.funcMetric(sPCTReturnMax[sPCTReturnMax.index>=dtStart])
        dictVarianceMin = Utils.funcMetric(sPCTVarianceMin[sPCTVarianceMin.index>=dtStart])
        dictClassic = Utils.funcMetric(sPCTClassic[sPCTClassic.index>=dtStart])
        dictReturnMax['Method'] = 'Max Ret'
        dictVarianceMin['Method'] = 'Min Var'
        dictClassic['Method'] = 'K=12,H=1'
        df = pd.DataFrame([dictClassic, dictReturnMax, dictVarianceMin])
        df['Start'] = 'Since' + dtStart.strftime('%Y%m%d')
        listDF.append(df)
    dfTable = pd.concat(listDF).set_index(['Start','Method']).sort_index()
    return dfTable

def texTableDumpComparison(dfTable, strFileAddress):
    #-- read data 
    listOut = ['Ret', 'Vol', 'DD', 'SR', 'ValueFinal']
    listPercentageFormat = ['Ret', 'Vol', 'DD']
    listDecimalFormat = list(set(listOut).difference(listPercentageFormat))
    dfTable = dfTable[listOut]
    dfTable[listPercentageFormat] = (dfTable[listPercentageFormat]*100).round(2).astype(str) + '%'
    dfTable[listDecimalFormat] = dfTable[listDecimalFormat].round(2).astype(str)

    #-- dump
    dfTable.to_latex(strFileAddress)
    return dfTable

