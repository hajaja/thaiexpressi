# -*- coding:utf-8 -*-
import pandas as pd
import datetime
import numpy as np
import pdb
import os, shutil
import random
from dateutil.parser import parse
from imp import reload

from . import ParamRange
reload(ParamRange)
from . import PARAMS
reload(PARAMS)
from . import DumpExcel
reload(DumpExcel)

############
# configuration
############
#pd.set_option('display.width', pd.util.terminal.get_terminal_size()[0])

############
# dfDetail
############
def funcGetContractDetail():
    listDict = []
    for SecuCode in PARAMS.listSecuAll:
        listDict.append({'SecuCode': SecuCode, 'multiplier': 1.})
    dfDetail = pd.DataFrame(listDict).set_index('SecuCode')
    dfDetail.loc[PARAMS.listSecuAgri, 'Industry'] = 'Agri'
    dfDetail.loc[PARAMS.listSecuEnergy, 'Industry'] = 'Energy'
    dfDetail.loc[PARAMS.listSecuMetal, 'Industry'] = 'Metal'
    return dfDetail
dfDetail = funcGetContractDetail()
###################
# dfExe
###################
if os.path.exists(PARAMS.strFileAddressExe):
    dfExe = pd.read_pickle(PARAMS.strFileAddressExe)
else:
    dfExe = None

def getTableExe(strColumn):
    df = dfExe[strColumn].reset_index().pivot_table(columns='SecuCode', index='TradingDay', values=strColumn, aggfunc=lambda x: ' '.join(x))
    return df

def getDFOneProduct(SecuCode, listColumn):
    if len(listColumn) == 1:
        df = dfExe.ix[SecuCode][listColumn[0]]
    else:
        df = dfExe.ix[SecuCode][listColumn]
    return df

############
# wrapper
############
def funcWriteExcel(df, excel_writer, sheet_name='Sheet1'):
    #DumpExcel.funcWriteExcel(df, excel_writer, sheet_name)
    df.to_excel(excel_writer, sheet_name=sheet_name)

############
# functions
############
def getTradingDataPoint_Commodity(dictDataSpec):
    return getTradingDataPoint_Commodity_File(dictDataSpec)

def getTradingDataPoint_Commodity_File(dictDataSpec):
    freq = dictDataSpec['freq']
    Secu = dictDataSpec['Secu']
    strFileAddressAll = PARAMS.strFileAddressExe
    dfXY = pd.read_pickle(strFileAddressAll)
    dfXY = dfXY.ix[Secu]
    dfXY = dfXY.rename(columns={
            'OpenPrice': 'Open',
            'HighPrice': 'High',
            'LowPrice': 'Low',
            'ClosePrice': 'Close',
            'TurnoverVolume': 'Volume',
            })
    dfXY.index.name = 'dtEnd'
    dfXY = dfXY[['Open', 'High', 'Low', 'Close', 'Volume', 'OpenRaw', 'boolDominantChange']].astype(np.float)
    return dfXY

###################
# rebalance date
###################
def generateTimeStandard():
    dfAll = dfExe.loc['Corn']['Close']
    return dfAll

def generateDTTestStartCalendarDay(NDayTest, NDayShift, NWeekStart):
    dfForDTTestStart = generateTimeStandard()
    dfForDTTestStart = dfForDTTestStart[dfForDTTestStart.index >= PARAMS.dtDataStart]
    listDTTestStart = []
    dtTestStart = dfForDTTestStart.index[0]
    for dt in dfForDTTestStart.index:
        if (dt - dtTestStart).days > NDayTest and dt.weekday() == NDayShift:
            dtTestStart = dt
            listDTTestStart.append(dtTestStart)
    # shift 1 week
    ret = []
    for dt in listDTTestStart:
        dtShifted = dt + datetime.timedelta(7 * NWeekStart, 0)
        for n in [0, 7]:
            dtShifted = dtShifted + datetime.timedelta(n, 0)
            if dtShifted in dfForDTTestStart.index:
                ret.append(dtShifted)
                break
    listDTTestStart = ret
    return listDTTestStart

def generateNthFriday(NthFriday, NMonthStart, NDayShift=4, NMonthTest=2):
    dfForDTTestStart = generateTimeStandard()
    listDTTestStart = []
    listDT = pd.date_range(PARAMS.dtDataStart, dfForDTTestStart.index.max())
    dfDTFull = pd.Series(range(0, len(listDT)), index=listDT)
    def funcThridFriday(df, NthFriday):
        dfTemp = df[df.index.weekday==NDayShift]
        if NthFriday == -1 or dfTemp.index.size <= NthFriday:
            ret = dfTemp.index[-1]
        else:
            ret = dfTemp.index[NthFriday]
        return ret
    sDTTestStart = dfDTFull.groupby(dfDTFull.index.strftime('%Y%m')).apply(funcThridFriday, NthFriday).dropna()
    sDTTestStart = sDTTestStart[NMonthStart::NMonthTest].tolist()
    ret = []
    for dt in sDTTestStart:
        for n in range(0, 10):
            dtTrading = dt + datetime.timedelta(n, 0)
            if dtTrading in dfForDTTestStart.index and dtTrading.weekday()==NDayShift:
                ret.append(dtTrading)
                break
    return ret

def generateNSecu():
    sSecuFirstTrading = dfExe.reset_index().groupby('SecuCode')['TradingDay'].first()

    dfSecuFirstTrading = pd.DataFrame(sSecuFirstTrading)
    dfSecuFirstTrading = dfSecuFirstTrading.reset_index().set_index('TradingDay').sort_index()
    dfSecuFirstTrading['NSecu'] = 1
    dfSecuFirstTrading['NSecu'] = dfSecuFirstTrading['NSecu'].cumsum()
    sNSecu = dfSecuFirstTrading['NSecu']
    dtToday = datetime.datetime.combine(datetime.datetime.now(), datetime.time(0,0))
    sNSecu.ix[dtToday] = np.nan
    sNSecu = sNSecu.resample('1D').last().ffill()
    sNSecu.index = sNSecu.index.to_pydatetime()
    return sNSecu


########################
# parameter set sweep
########################
def extractElementFromList(d):
    ret = {}
    listKException = ['Secu', 'listFileAddress']
    for k in d.keys():
        v = d[k]
        if k not in listKException:
            ret[k] = v[0]
        else:
            ret[k] = v

    return ret

def sweepParam(dictDefault, strParamSweep):
    """
    calculate performance of one daily return series
    
    Parameters:
    ----------
    * sPCT: daily return series
                                            
    Returns:
    -------
    * ret: including all the metrics and the daily return series
    """
    # case root directory
    dirParamSetRoot = '%s/%s/'%(PARAMS.dirOutput, strParamSweep)
    for dirToMake in [dirParamSetRoot]:
        if os.path.exists(dirToMake) is False:
            os.mkdir(dirToMake)
    dictDefault['dirParamSetRoot'] = dirParamSetRoot

    # sweep paramter range
    dictParamRange = ParamRange.dictStrategyParamRange[strParamSweep]
    listDictToExpand = [dictDefault]
    listKey = dictParamRange.keys()
    listKey = [x for x in listKey if x != 'Secu']   # a dict per case
    dictDefault['Secu'] = dictParamRange['Secu']
    for strParam in listKey:
        listValueParam = dictParamRange[strParam]
        listDictExpanded = []
        for vParam in listValueParam:
            for dictToExpand in listDictToExpand:
                dictOne = dict(dictToExpand)
                dictOne[strParam] = vParam
                listDictExpanded.append(dictOne)
        listDictToExpand = listDictExpanded
    listDictTaskSetting = listDictToExpand

    # exceptions
    listDictTaskSettingFiltered = []
    for dictOne in listDictTaskSetting:
        if 'NMonthStart' in dictOne and dictOne['NMonthStart'] >= dictOne['NMonthTest']:
            continue 
        listDictTaskSettingFiltered.append(dictOne)
    listDictTaskSetting = listDictTaskSettingFiltered

    # generate strCase
    listKey = np.sort(listKey).tolist()
    for dictOne in listDictTaskSetting:
        strCase = ''
        for k in listKey:
            v = dictOne[k]
            if k in dictParamRange.keys():
                strCase = strCase + str(k)[:2] + str(v) + '_'
        dictOne['strCase'] = strCase[:-1]
        # port directory, signal directory
        dirCaseRoot = '%s/%s/'%(dictOne['dirParamSetRoot'], dictOne['strCase'])
        dirCaseSignal = '%s/%s/'%(dirCaseRoot, 'Signal')
        dirCasePort = '%s/%s/'%(dirCaseRoot, 'Port')
        for dirToMake in [dirCaseRoot, dirCaseSignal, dirCasePort]:
            if os.path.exists(dirToMake) is False:
                os.mkdir(dirToMake)
        dictOne['dirCaseRoot'] = dirCaseRoot
        dictOne['dirCaseSignal'] = dirCaseSignal
        dictOne['dirCasePort'] = dirCasePort
    
    # determine boolStoploss
    for dictOne in listDictTaskSetting:
        if 'decimalStoploss' in dictOne and dictOne['decimalStoploss'] < 0.99:
            dictOne['boolStoploss'] = True
            
    return listDictTaskSetting

def getFileAddressForTopPort(strParamSweep):
    listDictPerCase = sweepParam(PARAMS.dictDataSpecTemplate, strParamSweep)
    listRet = []
    for dictPerCase in listDictPerCase:
        listFileAddress = []
        for SecuCode in dictPerCase['Secu']:
            strFileAddress = '%s/%s.pickle'%(dictPerCase['dirCaseSignal'], SecuCode)
            listFileAddress.append(strFileAddress)
        dictPerCase['listFileAddress'] = listFileAddress
        listRet.append(dictPerCase)
    return listRet

def sprawnDictPerCase(listDictPerCase):
    listRet = []
    for dictPerCase in listDictPerCase:
        for SecuCode in dictPerCase['Secu']:
            dictPerSecu = dict(dictPerCase)
            strFileAddress = '%s/%s.pickle'%(dictPerCase['dirCaseSignal'], SecuCode)
            dictPerSecu['strFileAddress'] = strFileAddress
            dictPerSecu['Secu'] = SecuCode
            listRet.append(dictPerSecu)
    return listRet

###################
# metric 
###################
def funcMetric(sPCT, boolOutColumn=False):
    """
    calculate performance of one daily return series
    
    Parameters:
    ----------
    * sPCT: daily return series
                                            
    Returns:
    -------
    * ret: including all the metrics and the daily return series
    """
    if sPCT.dropna().empty:
        return {}
    sValue = (sPCT+1).cumprod()
    ValueFinal = sValue.dropna().ix[-1]
    sMax = sValue.expanding().max()
    sDD = sValue / sMax - 1
    Ret = np.power(ValueFinal, 252./sPCT.index.size) - 1
    Vol = sPCT.std() * np.sqrt(252.)
    DD = -sDD.min()
    ret = {}
    ret['Ret'] = Ret
    ret['Vol'] = Vol
    ret['DD'] = DD
    ret['SR'] = Ret / Vol
    ret['CR'] = Ret / DD
    ret['ValueFinal'] = ValueFinal
    listOutCommon = ['Ret', 'Vol', 'DD', 'SR', 'CR', 'ValueFinal']
    return ret


def funcCalcPort(listStrParamSweep, dtBackTestStart=PARAMS.dtBackTestStart):
    """
    calculate performance of portfolio specified in listStrParamSweep
    
    Parameters:
    ----------
    * listStrParamSweep (list or str): which strategies are included. 
    the strategies available are listed in Common/ParamRange.py
    * dtBackTestStart (datetime): back test start date
                                            
    Returns:
    -------
    * dfRet: metrics of all strategies included in listStrParamSweep
    * sV: the daily value of the strategies
    """


    if type(listStrParamSweep) != list:
        listStrParamSweep = [listStrParamSweep]

    # get all dictPerTopPort
    listS = []
    for strParamSweep in listStrParamSweep:
        ld = getFileAddressForTopPort(strParamSweep)
        for dictPerTopPort in ld:
            strCase = dictPerTopPort['strCase']

            strFileAddressPrefix = dictPerTopPort['dirCasePort']
            sPortConfig = pd.read_pickle('%s/portconfig.pickle'%(strFileAddressPrefix))
            sDailyReturn = (pd.read_pickle('%s/dfOut.pickle'%(strFileAddressPrefix))['Cum Return'] + 1).pct_change()
            sDailyReturn.name = strCase
            # dtBackTestStart
            sDailyReturn = sDailyReturn[sDailyReturn.index >= dtBackTestStart]

            sPortConfig.ix['sDailyReturn'] = sDailyReturn
            sMetric = funcMetric(sDailyReturn.ix[:-1])
            sPortConfig = sPortConfig.append(pd.Series(sMetric))
            listS.append(sPortConfig.to_dict())
    
    # build common index
    ix = listS[0]['sDailyReturn'].index
    for dictOne in listS:
        ix = ix.union(dictOne['sDailyReturn'].index)
    for n, d in enumerate(listS):
        listS[n]['sDailyReturn'] = d['sDailyReturn'].ix[ix].fillna(0)
    
    # calculate daily return of port
    dfPCT = pd.DataFrame(listS)['sDailyReturn'].tolist()
    dfPCT = pd.concat(dfPCT, axis=1).ix[:-1]
    dfPCT['Port'] = dfPCT.mean(1)

    dfRet = pd.DataFrame(listS)
    dfRet = dfRet[dfRet.columns.drop('sDailyReturn')]
    
    # port value
    s = dfPCT['Port']
    sV = (s+1).cumprod()
    dfV = (dfPCT + 1).cumprod()

    # show the perofmrance 
    sMetric = funcMetric(s)
    print(sMetric)
    
    return dfRet, sV
    
def funcCalcPortKH(listStrParamSweep, dtBackTestStart=PARAMS.dtBackTestStart,
        tupleBestKH=None):
    """
    calculate performance of portfolio specified in listStrParamSweep, grouped
    by different Lookback Period and Holding Period
    
    Parameters:
    ----------
    * listStrParamSweep (list or str): which strategies are included. 
    the strategies available are listed in Common/ParamRange.py
    * dtBackTestStart (datetime): back test start date
                                            
    Returns:
    -------
    * dfRet: metrics of all strategies included in listStrParamSweep
    * sV: the daily value of the strategies
    """

    if type(listStrParamSweep) != list:
        listStrParamSweep = [listStrParamSweep]

    # get all dictPerTopPort
    listDictS = []
    for strParamSweep in listStrParamSweep:
        ld = getFileAddressForTopPort(strParamSweep)
        for dictPerTopPort in ld:
            strCase = dictPerTopPort['strCase']

            strFileAddressPrefix = dictPerTopPort['dirCasePort']
            sPortConfig = pd.read_pickle('%s/portconfig.pickle'%(strFileAddressPrefix))
            sDailyReturn = (pd.read_pickle('%s/dfOut.pickle'%(strFileAddressPrefix))['Cum Return'] + 1).pct_change()
            sDailyReturn.name = strCase
            # dtBackTestStart
            sDailyReturn = sDailyReturn[sDailyReturn.index >= dtBackTestStart]

            listDictS.append({
                'sPCT': sDailyReturn, 
                'NMonthLookBack': sPortConfig['NMonthTrain'],
                'NMonthHolding': sPortConfig['NMonthTest'],
                })
    dfPCT = pd.DataFrame(listDictS)
    def funcCalculateMetricKH(sSPCT):
        sPCT = pd.concat(sSPCT.values.tolist(), axis=1)
        sPCT = sPCT.mean(1)
        dictMetric = funcMetric(sPCT)
        ret = pd.DataFrame(pd.Series(dictMetric)).transpose()
        return ret
    dfMetric = dfPCT.groupby(['NMonthLookBack', 'NMonthHolding'])['sPCT'].apply(funcCalculateMetricKH)
   
    # best port
    if tupleBestKH is None:
        tupleBestKH = dfMetric['SR'].idxmax()[:2]
    listSPCT = dfPCT.set_index(['NMonthLookBack','NMonthHolding']).loc[tupleBestKH]['sPCT'].values.tolist()
    sPCT = pd.concat(listSPCT, axis=1).mean(1)
    sV = (1+sPCT).cumprod()
    
    return dfMetric, sV
    

#########################################
# keep operation
######################################### 
def keepOperation(s):
    s = s.ffill().fillna(0)
    indexNonOperation = s.diff()==0
    s[indexNonOperation] = np.nan
    return s

#########################################
# directory
######################################### 
def funcClearData(strParamSweep):
    ld = getFileAddressForTopPort(strParamSweep)
    for n, dictPerTopPort in enumerate(ld):
        if os.path.exists(dictPerTopPort['dirCaseSignal']):
            shutil.rmtree(dictPerTopPort['dirCaseSignal'])

if __name__ == '__main__':
    sNSecu = sNSecu

