# -*- coding:utf-8 -*-
import pandas as pd
from imp import reload
from . import PARAMS
reload(PARAMS)
listSecuAll = PARAMS.listSecuAll

listCloseAtDayEnd = [None]
dictStrategyParamRange = {}

############
# TSM
############
dictStrategyParamRange['TSM_Regression'] = {
       'Secu': listSecuAll,
       'strModelName': ['TSM'],
       'strMethodTrend': ['Simple'],
       'NMonthTest': [1,],
       'NMonthTrain': [12],
       'NDayShift': [0],
       'NDaySkip': [0,],
       'NMonthStart': [0,],
       'NthFriday': [0],
       'decimalStoploss': [0.99],
        }

dictStrategyParamRange['TSM_NthFriday'] = {
       'Secu': listSecuAll,
       'strModelName': ['TSM'],
       'strMethodTrend': ['Simple'],
       'NMonthTest': [1,],
       'NMonthTrain': [12],
       'NDayShift': [0, 2, 4],
       'NDaySkip': [0,],
       'NMonthStart': [0,],
       'NthFriday': [-1,0,1,2,3],
       'decimalStoploss': [0.99],
        }

dictStrategyParamRange['TSM_Classic'] = {
       'Secu': listSecuAll,
       'strModelName': ['TSM'],
       'strMethodTrend': ['Simple'],
       'NMonthTest': [1,],
       'NMonthTrain': [12],
       'NDayShift': [0],
       'NDaySkip': [0,],
       'NMonthStart': [0,],
       'NthFriday': [-1],
       'decimalStoploss': [0.99],
        }
dictStrategyParamRange['TSM_SensitivityReduction'] = dictStrategyParamRange['TSM_Classic']
dictStrategyParamRange['TSM_SectorLimit'] = dictStrategyParamRange['TSM_Classic']
dictStrategyParamRange['TSM_Async'] = dictStrategyParamRange['TSM_Classic']

dictStrategyParamRange['TSM_Small'] = {
       'Secu': listSecuAll,
       'strModelName': ['TSM'],
       'strMethodTrend': ['Simple'],
       'NMonthTest': [1,2,3],
       'NMonthTrain': [1,3,6,12],
       'NDayShift': [0],
       'NDaySkip': [0],
       'NMonthStart': [0,1,2],
       'NthFriday': [0],
       'decimalStoploss': [0.99],
        }

dictStrategyParamRange['TSM_Batch'] = {
       'Secu': listSecuAll,
       'strModelName': ['TSM'],
       'strMethodTrend': ['Simple'],
       'NMonthTest': [1,2,3,6],
       'NMonthTrain': [1,3,6,9,12],
       'NDayShift': [0,1,2,3,4],
       'NDaySkip': [0],
       'NMonthStart': [0,1,2],
       'NthFriday': [0,1,2,3],
       'decimalStoploss': [0.99],
        }

