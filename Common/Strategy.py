# -*- coding:utf-8 -*-
import pandas as pd
import datetime
import numpy as np
import pdb
import gc
from imp import reload

from . import PARAMS
reload(PARAMS)
from . import Utils
reload(Utils)

class Strategy:
    def __init__(self, df, dictParam):
        self.dictEvaluation = {}
        self.dictParam = dictParam
        self.df = df
        self.rfr = 0.03
        self.rfrDaily = np.power(self.rfr + 1, 1./PARAMS.NTradingDayPerYear) - 1
   
    def evaluateLongShortSimplified(self):
        # parameter 
        decimalCommission = self.dictParam['commission']
        if 'stoplossSlippage' in self.dictParam:
            decimalStoplossSlippage = abs(self.dictParam['stoplossSlippage'])
        else:
            decimalStoplossSlippage = 0.0

        if 'boolStoploss' in self.dictParam:
            boolStoploss = self.dictParam['boolStoploss']
            decimalStoploss = abs(self.dictParam['stoploss'])
        else:
            boolStoploss = False
        
        if 'boolStopProfit' in self.dictParam:
            boolStopProfit = self.dictParam['boolStopProfit']
            decimalStopProfit = abs(self.dictParam['stopProfit'])
        else:
            boolStopProfit = False
        
        # set indexLongEnter & indexShortEnter
        self.df['indicatorOfDecision'] = self.df['indicator']
        self.df['indicator'] = self.df['indicator'].shift(1)

        seriesIndicator = self.df['indicator'].copy()
        seriesIndicator = Utils.keepOperation(seriesIndicator.ffill())
        self.indexLongEnter = seriesIndicator[seriesIndicator==1].index
        self.indexShortEnter = seriesIndicator[seriesIndicator==-1].index

        seriesIndicatorNonShift = self.df['indicatorOfDecision'].ffill()
        self.indexLongClose = seriesIndicatorNonShift[(seriesIndicatorNonShift!=1)&(seriesIndicatorNonShift.shift(1)==1)].index
        self.indexShortClose = seriesIndicatorNonShift[(seriesIndicatorNonShift!=-1)&(seriesIndicatorNonShift.shift(1)==-1)].index


        if (self.indexLongEnter.size != 0) and self.indexLongEnter[-1] == self.df.index[-1]:
            self.indexLongEnter = self.indexLongEnter[:-1]
        if (self.indexShortEnter.size != 0) and self.indexShortEnter[-1] == self.df.index[-1]:
            self.indexShortEnter = self.indexShortEnter[:-1]
        
        # calculate daily return
        self.df['returnPCT'] = self.df['Close'].pct_change().fillna(0)
        self.df['returnPCTOpenToClose'] = self.df['Close'] / self.df['Open'] - 1
        self.df['returnPCTPrevCloseToOpen'] = self.df['Open'] / self.df['Close'].shift(1) - 1
        self.df['returnPCTPrevCloseToHigh'] = self.df['High'] / self.df['Close'].shift(1) - 1
        self.df['returnPCTPrevCloseToLow'] = self.df['Low'] / self.df['Close'].shift(1) - 1
        self.df['returnPCTOpenToHigh'] = self.df['High'] / self.df['Open'] - 1
        self.df['returnPCTOpenToLow'] = self.df['Low'] / self.df['Open'] - 1

        # stop loss
        if boolStoploss:
            self.stopLoss(decimalStoploss, decimalStoplossSlippage)
            self.df.ix[(self.df['Stoploss'].shift(1)==1)&(self.df['indicator'].isnull()), 'indicator'] = 0

        # stop profit
        if boolStopProfit:
            self.stopProfit(decimalStopProfit)
            self.df.ix[(self.df['StopProfit'].shift(1)==1)&(self.df['indicator'].isnull()), 'indicator'] = 0

        # ffill the indicator
        self.df['indicator'] = self.df['indicator'].ffill().fillna(0)

        # calculate metric
        self.df['returnPCTHold'] = self.df['returnPCT'] * self.df['indicator']

        # calculate metric - PrevCloseToOpen at operating day, valid if adjust position at open
        self.df.ix[self.indexLongEnter, 'returnPCTHold'] = self.df.ix[self.indexLongEnter, 'returnPCTOpenToClose'] * self.df['indicator'] - decimalCommission
        self.df.ix[self.indexShortEnter, 'returnPCTHold'] = self.df.ix[self.indexShortEnter, 'returnPCTOpenToClose'] * self.df['indicator'] - decimalCommission
        
        self.df.ix[self.indexLongClose, 'returnPCTHold'] = self.df.ix[self.indexLongClose, 'returnPCTHold'] + self.df['returnPCTPrevCloseToOpen'].shift(-1).ix[self.indexLongClose] * 1 - decimalCommission
        self.df.ix[self.indexShortClose, 'returnPCTHold'] = self.df.ix[self.indexShortClose, 'returnPCTHold'] + self.df['returnPCTPrevCloseToOpen'].shift(-1).ix[self.indexShortClose] * (-1) - decimalCommission

        # calculate metric - if stop loss
        if boolStoploss:
            self.df.ix[(self.df['Stoploss']==1), 'returnPCTHold'] = self.df.ix[(self.df['Stoploss']==1), 'returnStoploss']
        if boolStopProfit:
            self.df.ix[(self.df['StopProfit']==1), 'returnPCTHold'] = self.df.ix[(self.df['StopProfit']==1), 'returnStopProfit']

        # calculate metric - calculate metric
        seriesCumulatedValue = (self.df['returnPCTHold'] + 1).cumprod()
        seriesMaxUntilNow = seriesCumulatedValue.expanding().max()
        seriesMaxUntilNow = seriesMaxUntilNow.apply(lambda x: max(x, 1))
        seriesDD = (seriesMaxUntilNow - seriesCumulatedValue) / seriesMaxUntilNow
        seriesReturnPCTHoldDaily = self.df['returnPCTHold'].copy()
        def funcCalculateDailyReturn(s):
            return (s+1).cumprod()[-1] - 1
        seriesReturnPCTHoldDaily = seriesReturnPCTHoldDaily.groupby(seriesReturnPCTHoldDaily.index.date).apply(funcCalculateDailyReturn)
        self.seriesReturnPCTHoldDaily = seriesReturnPCTHoldDaily
        dictResult = {}
        dictResult['ReturnFinal'] = (seriesReturnPCTHoldDaily + 1).cumprod().dropna()[-1]
        dictResult['maxDD'] = seriesDD.max()
        dictResult['DTMaxDD'] = seriesDD.idxmax()
        dictResult['returnAnnualized'] = np.power(dictResult['ReturnFinal'], float(PARAMS.NTradingDayPerYear) / len(seriesReturnPCTHoldDaily)) - 1.
        dictResult['sigmaAnnualized'] = seriesReturnPCTHoldDaily.std() * np.sqrt(PARAMS.NTradingDayPerYear)
        dictResult['SharpeRatio'] = (seriesReturnPCTHoldDaily.mean()*PARAMS.NTradingDayPerYear - self.rfr) / (seriesReturnPCTHoldDaily.std() * np.sqrt(PARAMS.NTradingDayPerYear))
        dictResult['SharpeRatio'] = (seriesReturnPCTHoldDaily.mean()*PARAMS.NTradingDayPerYear - self.rfr) / (seriesReturnPCTHoldDaily.std() * np.sqrt(PARAMS.NTradingDayPerYear))
        self.dictEvaluation.update(dictResult)

        del seriesCumulatedValue, seriesMaxUntilNow, seriesDD
        gc.collect()

        return dictResult

