# -*- coding:utf-8 -*-
import pandas as pd
import numpy as np
import os, shutil, pdb, datetime, sys, gc, random
from dateutil.parser import parse
try:
    import docplex
    boolCplexAvailable = True
except:
    boolCplexAvailable = False
##############################################
# machine related
##############################################
import multiprocessing
ncpus = multiprocessing.cpu_count()
ncpus = min(ncpus-1, 8)

##############################################
# calendar
##############################################
dtDataStart = datetime.datetime(1970,1,1)
dtBackTestStart = datetime.datetime(1972,1,1)
dtBackTestStartReference = datetime.datetime(1985,1,1)
dtEnter = datetime.datetime(2013,1,1)
NTradingDayPerYear = 261

##############################################
# param
##############################################
COMMISSION = 0.0002         # 3CNY is cost when buying or selling a contract with contract value of x 10000CNY
TOTALMONEY = 2e6          # the total money can be invested
LEVERAGE = 1.0              # the contract value cannot be larger than TOTALMONEY * LEVERAGE
NThresholdVolume = 1000     # contracts with daily volume lower than 1000 cannot be traded 
boolClearData = True        # True: clear temporary data, to redo everything. 
UpperPositionSingleContract = 0.5

##############################################
# product
##############################################
listSecuAgri = ["Feeder Cattle","Live Cattle","Lean hogs", "Kansas wheat","Soybeans","Corn","Sugar","Cocoa","Coffee","Cotton",]
listSecuMetal = ["Silver","Gold","LME Nickel","LME Lead","LME Copper","LME Zinc","LME Aluminum",]
listSecuEnergy = ["Natural gas","Gasoline","Heating oil","Brent crude","WTI","Gas oil",]
listSecuAll = listSecuAgri + listSecuMetal + listSecuEnergy

dictSectorProduct = {}
dictSectorProduct['Cattle'] = ['Feeder Cattle', 'Live Cattle', 'Lean hogs']
dictSectorProduct['Wheat'] = ['Kansas wheat', 'Soybeans', 'Corn']
dictSectorProduct['Sugar'] = ['Sugar']
dictSectorProduct['Cocoa'] = ['Coffee']
dictSectorProduct['Cotton'] = ['Cotton']
dictSectorProduct['Gold'] = ['Gold', 'Silver']
dictSectorProduct['Metal'] = ['LME Nickel','LME Lead', 'LME Copper', 'LME Zinc', 'LME Aluminum']
dictSectorProduct['Gas'] = ['Natural gas', 'Gasoline', 'Heating oil', 'Brent crude', 'WTI', 'Gas oil']

##############################################
# directory
##############################################
dirProjectRoot = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
dirDataSource = dirProjectRoot + '/Data/'
dirOutput = dirProjectRoot + '/Output/'
for strDirToMake in [dirDataSource, dirOutput]:
    if os.path.exists(strDirToMake) is False:
        os.mkdir(strDirToMake)
strFileAddressExe = '%s/ExeCommodityFuture.pickle'%dirDataSource
strFileAddressXLSX = '%s/Commodity_Data.xlsx'%dirDataSource

#-- MFM directory
dirPortfolioTheory = '%s/%s/'%(dirOutput, 'PortfolioTheory')
for strDirToMake in [dirPortfolioTheory]:
    if os.path.exists(strDirToMake) is False:
        os.mkdir(strDirToMake)
strFileAddressReturnMax = '%s/%s.pickle'%(dirPortfolioTheory,'ReturnMax')
strFileAddressVarianceMin = '%s/%s.pickle'%(dirPortfolioTheory, 'VarianceMin')

##############################################
# tex 
##############################################
dirTex = '%s/Report/tex/'%(dirProjectRoot)
dirFigures = '%s/%s/'%(dirTex, 'figures')
dirTables = '%s/%s/'%(dirTex, 'tables')
#-- table file
strTableAddressSummaryStatistics = '%s/%s.txt'%(dirTables, 'SummaryStatistics')
strTableAddressKHSR = '%s/%s.txt'%(dirTables, 'KHSR')
strTableAddressKHSRPerSecu = '%s/%s.txt'%(dirTables, 'KHSRPerSecu')
strFileAddressKHSRPerSecu = '%s/%s.pickle'%(dirTables, 'KHSRPerSecu')
strFileAddressKHCRPerSecu = '%s/%s.pickle'%(dirTables, 'KHCRPerSecu')
strTableAddressMFM = '%s/%s.txt'%(dirTables, 'MFM')
strTableAddressSectorLimit = '%s/%s.txt'%(dirTables, 'SectorLimit')
#-- figure file
strFigureAddressEWIndex = '%s/%s.eps'%(dirFigures, 'EWIndex')
strFigureAddressCorrelationMatrix = '%s/%s.eps'%(dirFigures, 'CorrelationMatrix')
strFigureAddressStrategyPerf0 = '%s/%s.eps'%(dirFigures, 'StrategyPerf0')
strFigureAddressStrategyPerf1 = '%s/%s.eps'%(dirFigures, 'StrategyPerf1')
strFigureAddressStrategyPerf2 = '%s/%s.eps'%(dirFigures, 'StrategyPerf2')
strFigureAddressTSMSmileTemplate = '%s/%s{0}.eps'%(dirFigures, 'TSMSmile')
strFigureAddressSensitivityTemplate = '%s/%s{0}.eps'%(dirFigures, 'Sensitivity')
strFigureAddressContribPerProduct = '%s/%s.eps'%(dirFigures, 'ContribPerProduct')
strFigureAddressContribPerSector = '%s/%s.eps'%(dirFigures, 'ContribPerSector')
strFigureAddressMetricPerSecuTemplate = '%s/%s_{0}_{1}.eps'%(dirFigures, 'MetricPerSecu')
strFigureAddressValueComparisonTemplate = '%s/%s_{0}.eps'%(dirFigures, 'ValueComparison')

##############################################
# template
##############################################
dictParamTemplate = {
    'commission': COMMISSION * 2,
    'stoploss': 0.05,
    'stoplossSlippage': 0.001,
    'boolStopProfit': False,
    'stopProfit': 0.05,
    'switchPlot': False,
    }

dictDataSpecTemplate = {
    'boolStoploss': False,
    }

