#-*- coding:utf-8 -*-
import pandas
import numpy
import os
import multiprocessing
import pandas as pd
import numpy as np
import os,datetime,re,logging,time,shutil,sys
from dateutil.parser import parse

#------- find lowest level directory
def findDirectory(strDirTop, strFileType='dirLowest'):
    ret = []
    if strFileType == 'dirLowest':
        for root,dirs,files in os.walk(strDirTop):
            if not dirs:
                ret.append(root)
    else:
        for root,dirs,files in os.walk(strDirTop):
            for name in files:
                if name.endswith(strFileType):
                    ret.append('%s/%s'%(root,name))
    return ret

#------- divide chunk
def divideChunk(strDirTop, strDirectoryPrefix):
    # get list of directory
    listDirectory = findDirectory(strDirTop)

    # directory ~ chunk
    dictDictChunk = {}
    pat = re.compile(r'(?:.*)?\/([\d\w]+)\/?')  #(?:.*) is non-capturing group
    for nDirectory, strDirectory in enumerate(listDirectory):
        matched = pat.match(strDirectory)
        if matched is not None:
            groups = matched.groups()
            YYYYMMDD = groups[0]
            #ChunkKey = '%s_%s'%(YYYYMMDD, str(nDirectory))
            ChunkKey = YYYYMMDD
            listFile = []
            for root, dirs, files in os.walk(strDirectory):
                for name in files:
                    strFileAddress = '%s/%s'%(root, name)
                    listFile.append(strFileAddress)

            if dictDictChunk.has_key(ChunkKey):
                listFile = list(np.sort(dictDictChunk[ChunkKey]['listFile'] + listFile))
                dictDictChunk[ChunkKey]['listFile'] = listFile
            else:
                dictChunk = {'listFile': listFile, 'ChunkKey': ChunkKey, 'YYYYMMDD': YYYYMMDD}
                dictDictChunk[ChunkKey] = dictChunk

    # assign chunk file address, the existence of strDirectoryPrefix has to be assured outside
    for ChunkKey in dictDictChunk.keys():
        strFileAddressChunk = '%s/%s.pickle'%(strDirectoryPrefix, dictDictChunk[ChunkKey]['YYYYMMDD'])
        dictDictChunk[ChunkKey]['strFileAddressChunk'] = strFileAddressChunk

    return dictDictChunk

#------- check redudant chunk
def removeRedudantChunk(dictDictChunk):
    listChunkKey = np.sort([x for x in dictDictChunk.keys()])
    for ChunkKey in listChunkKey:
        dictChunk = dictDictChunk[ChunkKey]
        if os.path.exists(dictChunk['strFileAddressChunk']):
            print('redudant chunk %s'%ChunkKey)
            del dictDictChunk[ChunkKey]
    return dictDictChunk

#------- submit jobs
def submitjobs(dictDictChunk, dictOptionDefault):
    import multiprocessing
    # server parameter
    if 'ncpus' not in dictOptionDefault:
        ncpus = int(np.floor(multiprocessing.cpu_count() * 0.8))
        ncpus = max(1, ncpus)
    else:
        ncpus = dictOptionDefault['ncpus']

    #--- too few tasks
    NTask = len(dictDictChunk.keys())
    ncpus = min(NTask, ncpus)
    ppservers = ()

    # submit jobs
    listDictOption = []
    listChunkKey = list(np.sort([x for x in dictDictChunk.keys()]))
    NChunk = len(listChunkKey)
    for nChunk in range(0, NChunk):
        # build the chunk
        ChunkKey = listChunkKey[nChunk]
        dictChunk = dictDictChunk[ChunkKey]
        dictOption = dictChunk
        dictOption['ChunkKey'] = ChunkKey
        dictOption.update(dictOptionDefault)
        if 'funcTask' in dictOption.keys():
            del dictOption['funcTask']
        listDictOption.append(dictOption)
    
        # submit jobs every ncpus chunks 
        if nChunk%ncpus == ncpus-1 or nChunk == NChunk-1:
            pool = multiprocessing.Pool(processes=ncpus)
            pool.map(dictOptionDefault['funcTask'], listDictOption)
            pool.close()

